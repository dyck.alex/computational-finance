For answers to the questions in the exercises read the PDF in

	presentation/worksheet\ 1/sheet1.pdf
	presentation/worksheet\ 2/sheet2.pdf
	presentation/worksheet\ 3/sheet3.pdf
	presentation/worksheet\ 4/sheet4.pdf


Go to

	code/tasks/


The code can be found in src/

Under linux: Execute compile.sh 


Then the executable can be found in cmake-build-debug/ComputationalFinanceTasks
The documentation can be found in doc/

After execution one can copy the *.dat files to ext/data/ 
By executing the scripts in ext/ the plots can be found in ext/plots/


