cmake_minimum_required (VERSION 2.6)
project (ComputationalFinance)

set (Tutorial_VERSION_MAJOR 1)
set (Tutorial_VERSION_MINOR 0)

set(GCC_COVERAGE_COMPILE_FLAGS "-Wall -Wshadow -Werror -Wextra")
set(GCC_COVERAGE_LINK_FLAGS    "-lgsl -lgslcblas -lm")

file(GLOB_RECURSE ComputationalFinance_SOURCES "src/*.cpp")
file(GLOB_RECURSE ComputationalFinance_HEADERS "src/*.h")

set (ComputationalFinance_INCLUDE_DIRS "")
foreach (_headerFile ${ComputationalFinance_HEADERS})
    get_filename_component(_dir ${_headerFile} PATH)
    list (APPEND ComputationalFinance_INCLUDE_DIRS ${_dir})
endforeach()
list(REMOVE_DUPLICATES ComputationalFinance_INCLUDE_DIRS)


set( CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} ${GCC_COVERAGE_COMPILE_FLAGS}" )
set( CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} ${GCC_COVERAGE_LINK_FLAGS}" )

include_directories(${ComputationalFinance_INCLUDE_DIRS})

# add a target to generate API documentation with Doxygen
find_package(Doxygen)
if(DOXYGEN_FOUND)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/doxygenConfig.cfg ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY)
add_custom_target(doc
${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
COMMENT "Generating API documentation with Doxygen" VERBATIM
)
endif(DOXYGEN_FOUND)

add_executable (ComputationalFinance ${ComputationalFinance_SOURCES})