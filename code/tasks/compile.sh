#!/bin/bash
mkdir -p cmake-build-debug
cd cmake-build-debug
rm -rf *
cmake ..
make
make doc >/dev/null 2>&1
