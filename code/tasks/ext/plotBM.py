import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    data1 = np.loadtxt("./data/stepsize0_5.dat")
    data2 = np.loadtxt("./data/stepsize0_01.dat")
    wiener1 = np.loadtxt("./data/wiener0_5.dat").T[1:]
    wiener2 = np.loadtxt("./data/wiener0_01.dat").T[1:]

    time1 = data1.T[0]
    time2 = data2.T[0]
    data1 = data1.T[1:]
    data2 = data2.T[1:]

    fig = plt.figure()
    ax = fig.add_subplot(211)
    ax2 = fig.add_subplot(212)
    ax.plot(time1,data1.T, label=r"$\Delta t = 0.5$")
    ax.plot(time2,data2.T, label=r"$\Delta t = 0.01$")
    ax2.plot(time1,wiener1.T, label=r"$\Delta t = 0.5$")
    ax2.plot(time2,wiener2.T, label=r"$\Delta t = 0.01$")
    ax.set_title(r"Asset price  ")
    handles, labels = ax.get_legend_handles_labels()
    #ax.legend(handles, labels)

    ax2.set_title(r"Underlying Wiener process ")
    handles, labels = ax2.get_legend_handles_labels()
    #ax2.legend(handles, labels)
    plt.savefig("plots/geometricBrownianMotion.png")
