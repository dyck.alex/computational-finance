import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    data_mean = np.loadtxt("data/mean_estimation.dat")
    mu = data_mean[0][1:]
    N_mean = data_mean.T[0][1:]
    data_mean = data_mean.T[1:,1:]

    data_variance = np.loadtxt("data/sigma_estimation.dat")
    sigma = data_variance[0][1:]
    N_var = data_variance.T[0][1:]
    data_variance = data_variance.T[1:,1:]


    err = np.abs(np.array([mu-ii for ii in data_mean.T])).T
    err_sig = np.abs(np.array([sigma-ii for ii in data_variance.T])).T
    print(err,err_sig)
    fig = plt.figure()
    ax = fig.add_subplot(211)
    ax2 = fig.add_subplot(212)
    plt.ylim([1e-5,10])
    ax.loglog(N_mean, err[0], label = r"$\hat{\sigma}_1 = 1$")
    ax.loglog(N_mean, err[1], label = r"$\hat{\sigma}_2 = 1.5$")
    ax.loglog(N_mean, err[2], label =  r"$\hat{\sigma}_1 = 2.5$")
    ax.loglog(N_mean, np.ones(N_mean.shape)/np.sqrt(N_mean), label = r"$N^{-1/2}$")
    ax.set_title(r"estimation of $\mu = 2$ ")
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, labels, fontsize=8)

    ax2.loglog(N_var,err_sig[0], label = r"$\hat{\sigma}_1 = 1$")
    ax2.loglog(N_var,err_sig[1], label = r"$\hat{\sigma}_1 = 1.5$")
    ax2.loglog(N_var,err_sig[2], label = r"$\hat{\sigma}_1 = 2.5$")
    ax2.loglog(N_mean, np.ones(N_mean.shape)/np.sqrt(N_mean), label = r"$N^{-1/2}$")
    ax2.set_title(r"estimation of $\sigma $ with $ \mu = 2$ ")
    handles, labels = ax2.get_legend_handles_labels()
    ax2.legend(handles, labels, fontsize=8)
    plt.savefig("plots/estimation.png")
