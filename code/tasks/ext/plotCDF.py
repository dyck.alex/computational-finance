import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    data = np.loadtxt("data/CDF.dat")
    plt.hist(data)
    plt.savefig("plots/CDF.png")
