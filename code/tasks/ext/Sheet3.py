import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm

def task3():
	data = np.loadtxt("./data/s3t3.dat")
	N = data[:,0]
	err1 = data[:,1]
	err2 = data[:,2]

	fig = plt.figure()
	ax = fig.gca()
	ax.loglog(N,err1,color="blue",label = "M = 10")
	ax.loglog(N,err2,color="red",label = "M = 200")

	handles, labels = ax.get_legend_handles_labels()
	ax.legend(handles, labels, fontsize=8)

	plt.xlabel('$N$')
	plt.ylabel('relative error')

	plt.savefig("plots/s3t3.png")

def task4():
	data = np.loadtxt("./data/s3t4.dat")
	N = data[:,0]
	err1 = data[:,1]

	fig = plt.figure()
	ax = fig.gca()
	ax.loglog(N,err1)
	ax.loglog(N,[i**(-1.0) for i in N],color="black",linestyle="--",label = "$M^{-1}$" )

	handles, labels = ax.get_legend_handles_labels()
	ax.legend(handles, labels, fontsize=8)

	plt.xlabel('$M$')
	plt.ylabel('absolute error')

	plt.savefig("plots/s3t4.png")

def task5():
	data = np.loadtxt("./data/s3t5.dat")
	x = data[:,0]
	y = data[:,1]
	z = data[:,2]
	fig = plt.figure()
	ax = fig.gca(projection='3d')

	X = np.arange(1./50,1,1./50)
	Y = np.arange(1./50,1,1./50)
	Z = np.zeros((49, 49))
	k = 0
	for i in range(49):
	    for j in range(49):
		    Z[j][i] = z[k]
		    k+=1
	X, Y = np.meshgrid(X,Y)
	ax.plot_wireframe(X, Y, Z) # rstride=10, cstride=10)
	#ax.plot_surface(x,y,z, cmap=plt.cm.jet, rstride=1, cstride=1, linewidth=0)

	plt.savefig("plots/s3t5.png")
	plt.show()

def task7():
	data = np.loadtxt("data/s3t6_1.dat").T
	fig = plt.figure()
	ax = fig.add_subplot(121)
	ax.scatter(*data)
	ax.set_xlim([0, 1])
	ax.set_ylim([0, 1])
	ax.set_title("Uniform")
	ax.set(adjustable='box-forced', aspect='equal')
	data = np.loadtxt("data/s3t6_2.dat").T
	ax = fig.add_subplot(122)
	ax.scatter(*data)
	ax.set_xlim([0, 1])
	ax.set_ylim([0, 1])
	ax.set_title("Halton")
	ax.set(adjustable='box-forced', aspect='equal')
	plt.savefig("plots/s3t7.png")
	plt.show()

def task9():
	data = np.loadtxt("data/s3t9_1.dat").T
	fig = plt.figure()
	ax = fig.add_subplot(131)
	ax.scatter(*data)
	ax.set_xlim([0, 1])
	ax.set_ylim([0, 1])
	ax.set_title("Trapezoidal")
	ax.set(adjustable='box-forced', aspect='equal')
	data = np.loadtxt("data/s3t9_2.dat").T
	ax = fig.add_subplot(132)
	ax.scatter(*data)
	ax.set_xlim([0, 1])
	ax.set_ylim([0, 1])
	ax.set_title("Gauss-Legendre")
	ax.set(adjustable='box-forced', aspect='equal')
	data = np.loadtxt("data/s3t9_3.dat").T
	ax = fig.add_subplot(133)
	ax.scatter(*data)
	ax.set_xlim([0, 1])
	ax.set_ylim([0, 1])
	ax.set_title("Clenshaw Curtis")
	ax.set(adjustable='box-forced', aspect='equal')
	plt.savefig("plots/s3t9.png")
	plt.show()

def task11():
	data = np.loadtxt("data/s3t11_1.dat").T
	fig = plt.figure()
	ax = fig.add_subplot(221)
	ax.scatter(*data, s=5)
	ax.set_xlim([0, 1])
	ax.set_ylim([0, 1])
	ax.set_title("Trapezoidal for level=5")
	ax.set(adjustable='box-forced', aspect='equal')

	data = np.loadtxt("data/s3t11_2.dat").T
	ax = fig.add_subplot(222)
	ax.scatter(*data, s=5)
	ax.set_xlim([0, 1])
	ax.set_ylim([0, 1])
	ax.set_title("ClenshawCurtis for level=5")
	ax.set(adjustable='box-forced', aspect='equal')

	data = np.loadtxt("data/s3t11_3.dat").T
	ax = fig.add_subplot(223)
	ax.scatter(*data, s=5)
	ax.set_xlim([0, 1])
	ax.set_ylim([0, 1])
	ax.set_title("Trapezoidal, level=7")
	ax.set(adjustable='box-forced', aspect='equal')

	data = np.loadtxt("data/s3t11_4.dat").T
	ax = fig.add_subplot(224)
	ax.scatter(*data, s=5)
	ax.set_xlim([0, 1])
	ax.set_ylim([0, 1])
	ax.set_title("ClenshawCurti, level=7")
	ax.set(adjustable='box-forced', aspect='equal')
	plt.tight_layout()

	plt.savefig("plots/s3t11.png")
	plt.show()

def task12():
	data = np.loadtxt("./data/s3t12.dat")
	N = data[:,0]
	err1 = data[:,1]
	err2 = data[:,2]

	fig = plt.figure()
	ax = fig.gca()
	ax.semilogy(N,err2,color="blue",label = "tensor product rule")
	ax.semilogy(N,[16**i for i in N], color="blue",linestyle="--",label = "$16^d$")
	ax.semilogy(N,err1,color="red",label = "sparse grid")
	ax.semilogy(N,[16*2**(i-1) for i in N], color="red",linestyle="--",label = "$16\cdot 2^{d-1}$")

	handles, labels = ax.get_legend_handles_labels()
	ax.legend(handles, labels, fontsize=8)

	plt.xlabel('$d$')
	plt.ylabel('number of points')

	plt.savefig("plots/s3t12.png")
	plt.show()

def task13():
	# Picture 1
	data = np.loadtxt("./data/s3t13_0.dat")
	N = data[:,0]
	err1 = data[:,1]
	err2 = data[:,2]
	err3 = data[:,3]
	err4 = data[:,4]
	err5 = data[:,5]
	err6 = data[:,6]

	fig = plt.figure()
	ax = fig.gca()
	ax.loglog(N,err1, color="blue", label = 'Monte Carlo')
	ax.loglog(N,err2, color="black", label = 'Quasi Monte Carlo')
	ax.loglog(N,err3, color="green",label = 'product grid trapezoidal')
	ax.loglog(N,err4, color="red", label = 'product grid clenshaw curtis')
	ax.loglog(N,err5, color="yellow",linestyle="--", label = 'sparse grid trapezoidal')
	ax.loglog(N,err6, color="magenta",linestyle="--",label = 'sparse grid clenshaw curtis')

	ax.set_title("d=1")

	handles, labels = ax.get_legend_handles_labels()
	ax.legend(handles, labels, fontsize=6)

	#plt.tight_layout()
	plt.xlabel('$N$')
	plt.ylabel('relative error')

	plt.savefig("plots/s3t13_1.png")

	#Picture 2
	data = np.loadtxt("./data/s3t13_1.dat")
	N = data[:,0]
	err1 = data[:,1]
	err2 = data[:,2]
	err3 = data[:,3]
	err4 = data[:,4]
	err5 = data[:,5]
	err6 = data[:,6]

	fig = plt.figure()
	ax = fig.gca()
	ax.loglog(N,err1, color="blue", label = 'Monte Carlo')
	ax.loglog(N,err2, color="black", label = 'Quasi Monte Carlo')
	ax.loglog(N,err3, color="green",label = 'product grid trapezoidal')
	ax.loglog(N,err4, color="red", label = 'product grid clenshaw curtis')
	ax.loglog(N,err5, color="yellow",linestyle="--", label = 'sparse grid trapezoidal')
	ax.loglog(N,err6, color="magenta",linestyle="--",label = 'sparse grid clenshaw curtis')

	handles, labels = ax.get_legend_handles_labels()
	ax.legend(handles, labels, fontsize=6)
	ax.set_title("d=2")

	#plt.tight_layout()
	plt.xlabel('$N$')
	plt.ylabel('relative error')

	plt.savefig("plots/s3t13_2.png")

	#Picture 3
	data = np.loadtxt("./data/s3t13_2.dat")
	N = data[:,0]
	err1 = data[:,1]
	err2 = data[:,2]
	err3 = data[:,3]
	err4 = data[:,4]
	err5 = data[:,5]
	err6 = data[:,6]

	fig = plt.figure()
	ax = fig.gca()
	ax.loglog(N,err1, color="blue", label = 'Monte Carlo')
	ax.loglog(N,err2, color="black", label = 'Quasi Monte Carlo')
	ax.loglog(N,err3, color="green",label = 'product grid trapezoidal')
	ax.loglog(N,err4, color="red", label = 'product grid clenshaw curtis')
	ax.loglog(N,err5, color="yellow",linestyle="--", label = 'sparse grid trapezoidal')
	ax.loglog(N,err6, color="magenta",linestyle="--",label = 'sparse grid clenshaw curtis')
	ax.set_title("d=4")

	handles, labels = ax.get_legend_handles_labels()
	ax.legend(handles, labels, fontsize=6)

	plt.xlabel('$N$')
	plt.ylabel('relative error')
	plt.savefig("plots/s3t13_3.png")
	"""
	#Picture 4
	data = np.loadtxt("./data/s3t13_3.dat")
	N = data[:,0]
	err1 = data[:,1]
	err2 = data[:,2]

	fig = plt.figure()
	ax = fig.gca()
	ax.loglog(N,err1, label = 'Monte Carlo')
	ax.loglog(N,err2, label = 'Quasi Monte Carlo')
	ax.set_title("d=8")

	handles, labels = ax.get_legend_handles_labels()
	ax.legend(handles, labels, fontsize=8)

	plt.xlabel('$N$')
	plt.ylabel('relative error')

	plt.savefig("plots/s3t13_4.png")
	"""

def task15():
	data = np.loadtxt("./data/s3t115.dat")
	N = data[:,0]
	err1 = data[:,1]
	err2 = data[:,2]

	fig = plt.figure()
	ax = fig.gca()
	ax.loglog(N,err1, color="blue", label = 'random walk')
	ax.loglog(N,err2, color="red", label = 'brownian bridge')

	handles, labels = ax.get_legend_handles_labels()
	ax.legend(handles, labels, fontsize=6)

	plt.xlabel('$N$')
	plt.ylabel('relative error')

	plt.savefig("plots/s3t115.png")

def task17():
	data = np.loadtxt("./data/s3t17.dat")
	N = data[:,0]
	err1 = data[:,1]
	err2 = data[:,2]
	err3 = data[:,3]
	err4 = data[:,4]

	fig = plt.figure()
	ax = fig.gca()
	ax.loglog(N,err1, color="blue", label = 'mc random walk')
	ax.loglog(N,err2, color="green", label = 'mc brownian bridge')
	ax.loglog(N,err3, color="red", label = 'qmc random walk')
	ax.loglog(N,err4, color="magenta", label = 'qmc brownian bridge')
	ax.loglog(N,[i**(-0.5) for i in N],color="black", linestyle = '--', label = "N^(-0.5)")
	ax.loglog(N,[i**(-0.7) for i in N],color="grey", linestyle = '--', label = "N^(-0.7)")

	handles, labels = ax.get_legend_handles_labels()
	ax.legend(handles, labels, fontsize=6)

	plt.xlabel('$N$')
	plt.ylabel('relative error')

	plt.savefig("plots/s3t17.png")

task17()
