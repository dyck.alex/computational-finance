import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    data = np.loadtxt("data/boxmueller.dat").T
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(*data)
    plt.savefig("plots/boxmueller.png")
