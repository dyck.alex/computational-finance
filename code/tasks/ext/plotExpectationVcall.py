import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    data = np.loadtxt("./data/s2t4.dat")
    N = data[:,0]
    exp1 = data[:,1]
    exp2 = data[:,2]
    exp3 = data[:,3]
    exp4 = data[:,4]
    exp5 = data[:,5]

    fig = plt.figure()
    ax = fig.gca()
    ax.loglog(N,exp1)
    ax.loglog(N,exp2)
    ax.loglog(N,exp3)
    ax.loglog(N,exp4)
    ax.loglog(N,exp5)
    ax.loglog(N,[i**(-0.5) for i in N],color="grey",linestyle="--",label="$N^{-0.5}$")

    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, labels, fontsize=8)
    
    #ax.set_title(r"Expectation for Vcall convergence-plot ")
    plt.xlabel('$N$')

    plt.savefig("plots/s2t4.png")
