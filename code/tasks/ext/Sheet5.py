import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm

def task1():
	data = np.loadtxt("./data/s5t1.dat")
	x = data[:,0]
	y = data[:,1]
	z = data[:,2]
	fig = plt.figure()
	ax = fig.add_subplot(111,projection='3d')
	x = x.reshape((49,49))
	y = y.reshape((49,49))
	z = z.reshape((49,49))

	ax.plot_wireframe(x, y, z) # rstride=10, cstride=10)
	#ax.plot_surface(x,y,z)
	#ax.scatter(x,y,z)
	#ax.contour(x,y,z)
	plt.savefig("plots/s5t1.png")
	plt.show()

def task2():
	data = np.loadtxt("./data/s5t2.dat")
	N = data[:,0]
	err1 = data[:,1]
	err2 = data[:,2]

	fig = plt.figure()
	ax = fig.gca()
	ax.loglog(N,err1,color="blue",label = "Monte-Carlo")
	ax.loglog(N,[i**(-0.5) for i in N],color="black", linestyle = '--', label = "N^(-0.5)")
	ax.loglog(N,err2,color="red",label = "Quasi-Monte-Carlo")

	handles, labels = ax.get_legend_handles_labels()
	ax.legend(handles, labels, fontsize=8)

	plt.xlabel('$N$')
	plt.ylabel('relative error')

	plt.savefig("plots/s5t2.png")

def task3():
	data = np.loadtxt("./data/s5t3.dat")
	N = data[:,0]
	err1 = data[:,1]

	fig = plt.figure()
	ax = fig.gca()
	ax.plot(N,err1,color="blue")

	plt.xlabel('$B$')
	plt.ylabel('fair price')

	plt.savefig("plots/s5t3.png")

def task4():
	data = np.loadtxt("./data/s5t4.dat")
	N = data[:,0]
	err1 = data[:,1]
	err2 = data[:,2]
	err3 = data[:,3]
	err4 = data[:,4]

	fig = plt.figure()
	ax = fig.gca()
	ax.loglog(N,err1,color="blue",label = "M = 4")
	ax.loglog(N,[i**(-0.5) for i in N],color="black", linestyle = '--', label = "N^(-0.5)")
	ax.loglog(N,err2,color="red",label = "M = 64")
	ax.loglog(N,err3,color="green",label = "M = 256")
	ax.loglog(N,err4,color="magenta",label = "M = 1024")

	handles, labels = ax.get_legend_handles_labels()
	ax.legend(handles, labels, fontsize=8)

	plt.xlabel('$N$')
	plt.ylabel('relative error')

	plt.savefig("plots/s5t4.png")


def task5():
	data = np.loadtxt("./data/s5t5.dat")
	x = data[:,0]
	y = data[:,1]
	z = data[:,2]
	fig = plt.figure()
	ax = fig.add_subplot(111,projection='3d')
	x = x.reshape((49,49))
	y = y.reshape((49,49))
	z = z.reshape((49,49))

	ax.plot_wireframe(x, y, z) # rstride=10, cstride=10)
	#ax.plot_surface(x,y,z)
	#ax.scatter(x,y,z)
	#ax.contour(x,y,z)
	plt.savefig("plots/s5t5.png")
	plt.show()


def task6():
	data = np.loadtxt("./data/s5t6.dat")
	N = data[:,0]
	err1 = data[:,1]
	err2 = data[:,2]

	fig = plt.figure()
	ax = fig.gca()
	ax.loglog(N,err1,color="blue",label = "Monte-Carlo")
	ax.loglog(N,err2,color="red",label = "Quasi-Monte-Carlo")

	handles, labels = ax.get_legend_handles_labels()
	ax.legend(handles, labels, fontsize=8)

	plt.xlabel('$N$')
	plt.ylabel('relative error')

	plt.savefig("plots/s5t6.png")
	plt.show()

def task7():
	data = np.loadtxt("./data/s5t7.dat")
	N = data[:,0]
	err1 = data[:,1]
	err2 = data[:,2]
	err3 = data[:,3]
	err4 = data[:,4]

	fig = plt.figure()
	ax = fig.gca()
	ax.loglog(N,err1,color="blue",label = "Monte-Carlo")
	ax.loglog(N,err2,color="red",label = "Quasi-Monte-Carlo")
	ax.loglog(N,err3,color="blue", ls = '-',label = "Monte-Carlo with variate")
	ax.loglog(N,err4,color="red",ls = '-',label = "Quasi-Monte-Carlo with variate")

	handles, labels = ax.get_legend_handles_labels()
	ax.legend(handles, labels, fontsize=8)

	plt.xlabel('$N$')
	plt.ylabel('relative error')

	plt.savefig("plots/s5t7.png")
	plt.show()

def task8():
	data = np.loadtxt("./data/s5t8.dat")
	N = data[:,0]
	vol = data[:,1]

	fig = plt.figure()
	ax = fig.gca()
	ax.plot(N,vol,color="blue")

	plt.xlabel('$K$')
	plt.ylabel('implied volatility')

	plt.savefig("plots/s5t8.png")
	plt.show()
def task9():
	data = np.loadtxt("./data/s5t9_tesla.dat")
	N = data[:,0]
	vol = data[:,1]

	fig = plt.figure()
	ax = fig.gca()
	ax.plot(N,vol,color="blue")

	plt.xlabel('$K$')
	plt.ylabel('implied volatility')
	plt.title("Tesla Options")
	plt.savefig("plots/s5t9_tesla.png")
	plt.show()



task9()
