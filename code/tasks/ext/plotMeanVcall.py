import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    data = np.loadtxt("./data/s2t1.dat")

    sigma = data[:,0]
    mean = data[:,1]

    fig = plt.figure()
    ax = fig.gca()
    ax.plot(sigma,mean,'ro')
    #ax.set_title(r"Mean estimates for Vcall  ")
    plt.xlabel('$\sigma$')

    plt.savefig("plots/s2t1.png")
