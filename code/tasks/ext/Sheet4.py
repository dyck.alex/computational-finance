import numpy as np
import matplotlib.pyplot as plt

def task11():
    data = np.loadtxt("data/s4t1.dat")
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(data)

def task12():
    data = np.loadtxt("data/s4t1.dat")
    time = np.linspace(0,2,33)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_ylabel(r"S(t)")
    ax.set_xlabel(r"t")
    print(time, len(data))
    for y in data:
        ax.scatter(time[y>0],y[y>0], marker='+')


def task2():
    data = np.loadtxt("data/s4t2.dat").T

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(data[0], data[1], label = "convergence rate")
    ax.plot(data[0], 1./(np.power(data[0],1.)) , label = r"$N^{-1}$")
    ax.loglog()
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, labels)

def task3():
    data = np.loadtxt("data/s4t3.dat").T
    S = data[0]
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(S, data[1], label = "European Put")
    ax.plot(S, data[2], label = "American Put")
    ax.plot(S, data[3], label = "Payoff function")
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, labels)

def task4():
    data = np.loadtxt("data/s4t4.dat").T
    M = data[0]
    fig = plt.figure()
    ax = fig.add_subplot(121)
    ax2  =fig.add_subplot(122)
    ax.plot(M, data[1], label = "Put - BG Algorithm", c='b', ls = '--')
    ax.plot(M, data[2], label = "Put - Bin method",c = 'b', ls='-')
    ax.plot(M, np.abs(data[3]), label = "Put - Difference", c = 'b', ls= ':')
    ax2.plot(M, data[3], label = "Call - BG Algorithm", c='g', ls = '--')
    ax2.plot(M, data[4], label = "Call - Bin method",c = 'g', ls='-')
    ax2.plot(M, np.abs(data[5]), label = "Call - Difference", c = 'g', ls= ':')
    #ax.loglog()
    #ax2.loglog()
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, labels,framealpha=0.5,fancybox=True)
    andles, labels = ax2.get_legend_handles_labels()
    ax2.legend(handles, labels,framealpha=0.5,fancybox=True)

def task42():
    data = np.loadtxt("data/simulation_Call.dat").T
    data2 = np.loadtxt("data/simulation_Put.dat").T
    t = (data[0]+1)/2
    fig = plt.figure()
    ax = fig.add_subplot(211)
    ax2 = fig.add_subplot(212)

    ax.scatter(t, data[1], c = 'b', marker='+')
    ax.scatter(0, 10, c = 'b', marker='+')
    ax.scatter(t, data[2], c = 'b', marker= '+')
    ax.set_title(r"Simulation of $S_{Call}$ option")
    ax.set_xlabel(r"$t$")
    ax.set_ylabel(r"$S_t^k$")
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, labels)
    ax2.scatter(t, data2[1], c = 'b', marker='+')
    ax2.scatter(0, 10, c = 'b', marker='+')
    ax2.scatter(t, data[2], c = 'b', marker= '+')
    ax2.set_ylabel(r"$S_t^k$")
    ax2.set_title(r"Simulation of $S_{Put}$ option")
    handles, labels = ax2.get_legend_handles_labels()
    ax2.legend(handles, labels)

if __name__ == "__main__":
    """
    task11()
    plt.savefig("plots/s4t11.png")
    task12()
    plt.savefig("plots/s4t12.png")
    """
    task2()
    plt.savefig("plots/s4t2.png")
    task3()
    plt.savefig("plots/s4t3.png")
    """
    task4()
    plt.savefig("plots/s4t4.png")
    task42()
    plt.savefig("plots/s4t42.png")
    """
