import numpy as np
import matplotlib.pyplot as plt

def print_plot(s):
    data = np.loadtxt("./data/"+s+".dat")
    N = data[:,0]
    err1 = data[:,1]
    err2 = data[:,2]
    err3 = data[:,3]
    err4 = data[:,4]

    fig = plt.figure()
    ax = fig.gca()
    ax.loglog(N,err1,color="blue",label = "Monte-Carlo")
    ax.loglog(N,err2,color="red",label = "Trapezoidal")
    ax.loglog(N,err3,color="green",linestyle="-", label = "Clenshaw-Curtis")
    ax.loglog(N,err4,color="grey",label = "Gauss-Legendre")
    ax.loglog(N,[i**(-2) for i in N],color="black",linestyle="--",label = "$N^{-2}$" )
    #ax.loglog(N,[i**(-10) for i in N],color="green",linestyle="--",label = "$R_{CC}^{N_l}$" )
    #plt.xscale("log")
    #ax.set_title(r"Relative errors of quadrature rules ")

    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, labels, fontsize=8)

    plt.xlabel('$N$')
    plt.ylabel('$\hat{R}^N(f)$')
    plt.ylim((10**(-17),10**0))

    plt.savefig("plots/"+s+".png")

if  __name__ == "__main__":
    print_plot("s2t9")
