cmake_minimum_required (VERSION 3.1)
project (ComputationalFinanceTasks)

set (Tutorial_VERSION_MAJOR 0)
set (Tutorial_VERSION_MINOR 1)

file(GLOB_RECURSE ComputationalFinanceTasks_SOURCES "src/*.cpp")
file(GLOB_RECURSE ComputationalFinanceTasks_HEADERS "src/*.h")

set (ComputationalFinanceTasks_INCLUDE_DIRS "")
foreach (_headerFile ${ComputationalFinanceTasks_HEADERS})
    get_filename_component(_dir ${_headerFile} PATH)
    list (APPEND ComputationalFinanceTasks_INCLUDE_DIRS ${_dir})
endforeach()
list(REMOVE_DUPLICATES ComputationalFinanceTasks_INCLUDE_DIRS)

set(GCC_COVERAGE_COMPILE_FLAGS "-std=c++11 -Wall -Wshadow -Werror -Wextra -pedantic")
set(GCC_COVERAGE_LINK_FLAGS    "-lgsl -lgslcblas -lm")

set( CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} ${GCC_COVERAGE_COMPILE_FLAGS}" )
set( CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} ${GCC_COVERAGE_LINK_FLAGS}" )

include_directories(${ComputationalFinanceTasks_INCLUDE_DIRS})

# add a target to generate API documentation with Doxygen
find_package(Doxygen)
if(DOXYGEN_FOUND)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/doxygenConfig.cfg ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY)
add_custom_target(doc
${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
COMMENT "Generating API documentation with Doxygen" VERBATIM
)
endif(DOXYGEN_FOUND)

add_executable (ComputationalFinanceTasks ${ComputationalFinanceTasks_SOURCES})

#target_compile_features(ComputationalFinanceTasks PRIVATE cxx_range_for)
