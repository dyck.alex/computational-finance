/*! \brief     Execution of all tasks in chronological order
 *  \date      09. May 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */

#include "./tasks.h"
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>
#include <time.h>

/**
 *
 * Main function for random number evaluation.
 * A first random number \f$x_1\f$ is drawn via rand. 
 * A second random number \f$x_2\f$ is drawn via gsl_rng_uniform.
 *
 * Furthermore, an array of 10 doubles is allocated. However, someone 
 * seems to have forgotten to free the allocated space again in the end...
 *
 * @param argc The number of arguments provided.
 * @param argv An array of arguments (argv[0] is the name of the
 * executable).
 *
 * @return If everything worked fine, 0 is returned.
*/
int main() {
    //sheet1_task1();
    //sheet1_task2();
    //sheet1_task4();
    //sheet1_task6();
    //sheet1_task8();
    //sheet1_task9();
    //sheet1_task10();
    //sheet1_task11();

    //sheet2_task1();
    //sheet2_task2();
    //sheet2_task4();
    //sheet2_task6();
    //sheet2_task7();
    //sheet2_task8();
    //sheet2_task9();
    //sheet2_task10();

    //sheet3_task3();
    //sheet3_task4();
    //sheet3_task5();
    //sheet3_task6();
    //sheet3_task7();
    //sheet3_task8();
    //sheet3_task9();
    //sheet3_task10();
    //sheet3_task11();
    //sheet3_task12();
    //sheet3_task13();
    //sheet3_task14();
    //sheet3_task15();
    //sheet3_task16();
    //sheet3_task17();

    //sheet4_task1();
    //sheet4_task2();
    //sheet4_task3();
    //sheet4_task4();

    //sheet5_task1();
    //sheet5_task2();
    //sheet5_task3();
    //sheet5_task4();
    //sheet5_task5();
    //sheet5_task6();
    //sheet5_task7();
    //sheet5_task8();
    sheet5_task9();
    return 0;
}

// enable doxygen processing for this header:
/** @file */
