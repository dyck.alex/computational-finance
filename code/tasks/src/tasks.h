// enable doxygen processing for this header:
/** @file */


/*! \brief     header to tasks.cpp
 *  \date      09. May 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */

#include <vector>
#include <utility>
#include <gsl/gsl_rng.h>
#include <fstream>
#include <list>
#include <functional>

#ifndef TASKS_H
#define TASKS_H


//typedefs
typedef std::pair<double,double> twodRV;

//shared
/**
 *
 * @tparam T Type of container.
 * @param data container itself
 * @param filename Filename where data is printed to
 */
template <class T> void printToFile(T data , const char * filename) {
    std::ofstream f;
    f.open(filename);
    for (auto const& val : data)
        f << val << "\n";
    f.close();
}

//Task 1
void sheet1_task1();
double randomNumber01();
double randomNumber01GSL(gsl_rng* r);	

//Task 2
void sheet1_task2();
double randomNumberab(double a, double b);
double randomNumberabGSL(gsl_rng * r, double a, double b);
double standardNormalDensity(double x);
double rejectionSampling(gsl_rng *, double, double);

//Task 4
void sheet1_task4();
double NormalCDF(double x);
double NormalInverseCDF(double x);
double moro_sampling(gsl_rng*);

//Task 6
void sheet1_task6();
twodRV boxMueller(gsl_rng * );

//Task 8
void sheet1_task8();
double empiricalMean(const std::vector<double>&);
double empiricalDeviation(const std::vector<double>& );
double empiricalDeviation_naive(const std::vector<double>& );

//Task 9
void sheet1_task9();

//Task 10
void sheet1_task10();
void WienerProcess(gsl_rng * r, const std::vector <double> & time, std::vector<double> & process);
void create_equidistant_timesteps(std::vector <double> & time_points, double T, double timestep);
void geometric_brownian_motion(const std::vector<double> & time,std::vector <double> & process, double S0, double mu, double sigma);

//Task 11
void sheet1_task11();

//Sheet 2
//Task 1
double payoff_call(std::vector <double> & process,double K,double T);
void sheet2_task1();

//Task 2
void sheet2_task2();

//Task 4
double expectation_Vcall_exact(double S0, double mu, double sigma, double T, double K);
void sheet2_task4();

//Task 6
void trapezoidal_rule(int l, std::vector<double> & nodes, std::vector<double> & weights);
void sheet2_task6();

//Task 7
void gauss_legendre_rule(int l,std::vector<double> & nodes,std::vector<double> & weights,double,double);
double polynomial(std::vector<double> coefficients, double x);
void sheet2_task7();

//Task 8
void clenshaw_curtis_rule(int l, std::vector<double> & nodes, std::vector<double> & weights);
void sheet2_task8();

//Task 9
void monte_carlo_rule(int l, std::vector<double> & nodes);
void sheet2_task9();

//Task 10
double fcall(double S0, double mu, double sigma, double T, double K, double s);
void sheet2_task10();

//Sheet 3
//Task 2
namespace Asian{
		double Vcall_discrete(double S0, double r, double sigma, double T, double K, double timestep, size_t M);
		double Vcall_continuous(double S0, double r, double sigma, double T, double K);
		//Task 3	
		double discrete_geometric_average(const std::vector<double> & process, double K);
		//Task 5
		double discrete_arithmetic_average(const std::vector<double> & process, double K);
}
void sheet3_task3();

//Task 4
void sheet3_task4();

//Task 5
double integrand_2d(double S0, double r, double sigma, double K, double s1, double s2);
void sheet3_task5();

//Task 6
void van_der_corput(const std::vector<double> & sequence, size_t p, size_t n);
void prime_numbers(std::vector<double> & sequence, size_t d);
void halton(std::vector< std::vector<double> > & sequence, size_t d, size_t n);
void sheet3_task6();

//Task 7
void sheet3_task7();

//Task 8
//double tensor_product_quadrature(double (*func)(double, std::vector<double>,std::vector<size_t>),std::vector<double> nodes,std::vector<double> weights, size_t d, double gamma);
void tensor_product_quadrature(size_t level, size_t d, std::vector< std::vector<double> > & nodes,std::vector<double> & weights,
                               const std::vector<double>& nodes1d, const std::vector<double> & weights1d);
double tensor_product_quadrature(size_t level, size_t d, const std::vector<double> & nodes1d, const std::vector<double> & weights1d,
                                 std::function<double (std::vector<double>) > func);
double linear_function(std::vector<double> values);
void sheet3_task8();

//Task 9
void sheet3_task9();

//Task 10
void sparse_grid_quadrature(size_t level, size_t d, std::vector< std::vector<double> > & nodes,std::vector<double> & weights, int rule);
double sparse_grid_quadrature(size_t level, size_t d, std::function<double (std::vector<double> ) > func, int rule);
void sheet3_task10();

//Task 11
void sheet3_task11();

//Task 12
void sheet3_task12();

//Task 13
double test_function(double gamma, std::vector<double> values);
void monte_carlo_rule_multidimensional(size_t l, size_t d, std::vector< std::vector<double> > & nodes);
void sheet3_task13();

//Task 14
void time_generate(std::vector <double> & time, double T, size_t M);
int find_index(double point, std::vector <double> & time);
void WienerProcess_BR(std::vector <double> nodes, std::vector <double> & time, std::vector<double> & BR, double T, size_t M);
void sheet3_task14();

//Task 15
double integrand_discrete_geometric_average(
        const std::vector <double> & time ,
        const std::vector <double> & process,
        double S0,
        double r ,
        double sig,
        double K );
void sheet3_task15();

//Task 16
void sheet3_task16();

//Task 17
void sheet3_task17();

//Sheet 4
//Task 1
double BinomialMethod_EuropeanCall(std::vector<std::vector<double> > &V, std::vector<std::vector<double> > &S,
								   double S0, double K, double sigma, double r, double M, double delta_t);
void sheet4_task1();

//Task 2
double BlackScholesFormula(double S0, double K, double sigma, double r, double T);
void sheet4_task2();

//Task 3
double BinomialMethod_AmericanPut(std::vector<std::vector<double> > &V, std::vector<std::vector<double> > &S,
                                  double S0, double K, double sigma, double r, double M, double delta_t);
double BinomialMethod_EuropeanPut(std::vector<std::vector<double> > &V, std::vector<std::vector<double> > &S,
                                  double S0, double K, double sigma, double r, double M, double delta_t);
void sheet4_task3();

//Task 4
void sheet4_task4();

//Sheet 5 (or "normal 4")

//Task 1
double discrete_downout_call(std::vector <double> & process, double K, double B);
void sheet5_task1();

//Task 2
void sheet5_task2();
void create_GBM(std::vector <double> & process, double S0, double r, double sigma, const std::vector <double> & nodes, const std::vector<double> & time);

//Task 3
double d_SK(double S, double K, double r, double sigma, double T);
double down_out_call(double S0, double r, double sigma, double K, double B, double T);
void sheet5_task3();

//Task 4
void sheet5_task4();

//Task 5
double discrete_lookback_call_fixed(const std::vector <double> & process, double K);
double discrete_lookback_call_variable(const std::vector <double> & process, double T);
void random_walk(const std::vector<double> & time, std::vector <double> & nodes);
void sheet5_task5();

//Task 6
double lookback_call(double S0, double r, double sigma, double T, double K);
double lookback_call(double S0, double r, double sigma, double T);
double integrand_discrete_lookback_call(const std::vector <double> & time, const std::vector <double> & process, double S0, double r , double sig, double K );
void sheet5_task6();

//Task 7
void sheet5_task7();

//Task 8
double Newton_Raphson_algorithm(double V, double S0, double r , double sigma0, double K , double T);
void sheet5_task8();

//Task 9
double European_Call(double S0, double K , double r, double sigma, double M,double dt);
void sheet5_task9();
#endif

