/*!
 *  \date      9. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>

void sheet3_task9(){
    std::vector<double> nodes1, weights1;
    std::vector<double> nodes2, weights2;
    std::vector<double> nodes3, weights3;

    int level = 5;
    trapezoidal_rule(level, nodes1, weights1);
    gauss_legendre_rule(level,nodes2,weights2, 0.,1.);
    clenshaw_curtis_rule(level,nodes3,weights3);

    std::ofstream f, f2, f3;
    f.open("s3t9_1.dat");
    f2.open("s3t9_2.dat");
    f3.open("s3t9_3.dat");
    for(size_t i=0; i < nodes1.size(); i++){
        for(size_t j=0; j < nodes1.size(); j++){
             f << nodes1[i] << " " << nodes1[j] << '\n';
             f2 << nodes2[i] << " " << nodes2[j] << '\n';
             f3 << nodes3[i] << " " << nodes3[j] << '\n';
        }
    }
    f.close();
    f2.close();
    f3.close();
}

// enable doxygen processing for this header:
/** @file */
