/*!
 *  \date      26. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <gsl/gsl_rng.h>
#include <cmath>
#include <iostream>
#include <sstream>

void sheet3_task17() {    
    gsl_rng* ran = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(ran,time(NULL));

    double S0 = 10.0 , r = 0.1 , sigma = 0.25 , T=1 , M= 2 , K =10.0;

    size_t d = M;
    std::ofstream f;
    f.open("s3t17.dat");
    double exact_value = Asian::Vcall_discrete(S0,r,sigma,T,K,T/M,M);
    std::cout << "exact: " << exact_value << '\n'; 
    //create time steps for random walk and brownian bridge
    std::vector<double> time_RW, time_BR, time_help, process, BR;
    create_equidistant_timesteps(time_RW, T, T/M);
    time_generate(time_BR,T,M);

	process.resize(M);

    for(size_t l = 1; l < 20; l++){

		std::cout << "level " << l << '\n';
		double mc_rw = 0 , mc_bb = 0, qmc_rw = 0, qmc_bb =0;
    	size_t N = pow(2,l) - 1;
        std::vector< std::vector<double> > nodes, nodes2;
		std::vector<double> weights;

        //Monte Carlo
        monte_carlo_rule_multidimensional(l, d, nodes);
        nodes2 = nodes;
        for (size_t i = 0; i < N ; i++) {
            random_walk(time_RW, nodes[i]);
            create_GBM(process, S0, r, sigma, nodes[i], time_RW);
            mc_rw += Asian::discrete_geometric_average(process,K);

            time_help = time_BR;
            WienerProcess_BR(nodes2[i], time_BR, BR, T, M);
            nodes2[i] = BR;
            create_GBM(process, S0, r, sigma, nodes2[i], time_BR);

            mc_bb += Asian::discrete_geometric_average(process,K);
            time_BR = time_help;
        }
        mc_rw /= N*std::exp(r*T);
        //std::cout << mc_rw << '\n';
        mc_rw = std::abs((exact_value - mc_rw)/exact_value);
        mc_bb /= N*std::exp(r*T);
        mc_bb = std::abs((exact_value - mc_bb)/exact_value);

        //Quasi Monte Carlo
        halton(nodes, d, N);
        nodes2 = nodes;
        for (size_t i = 0; i < N ; i++) {
            random_walk(time_RW, nodes[i]);
            create_GBM(process, S0, r, sigma, nodes[i], time_RW);
            qmc_rw += Asian::discrete_geometric_average(process,K);

            time_help = time_BR;
            WienerProcess_BR(nodes2[i], time_BR, BR, T, M);
            nodes2[i] = BR;
            create_GBM(process, S0, r, sigma, nodes2[i], time_BR);
            qmc_bb += Asian::discrete_geometric_average(process,K);
            time_BR = time_help;
        }
        qmc_rw /= N*std::exp(r*T);
        //std::cout << qmc_rw << '\n';
        qmc_rw = std::abs((exact_value - qmc_rw)/exact_value);
        qmc_bb /= N*std::exp(r*T);
        qmc_bb = std::abs((exact_value - qmc_bb)/exact_value);

        f << N << ' ' << mc_rw<< ' ' << mc_bb << ' ' << qmc_rw << ' ' << qmc_bb <<'\n';

    }
    f.close();
}

