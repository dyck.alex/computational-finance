/*!
 *  \date      8. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>
#include <functional>

// ugly implementation using gamma ...
/**
 *
 * @param func integrand
 * @param nodes nodes we are using to discretize the integral
 * @param weights weights corresponding to our nodes
 * @param d dimension
 * @param gamma
 * @return returns the integral computed via tensor product quadrature
 */
/*
double tensor_product_quadrature(double (*func)(double, std::vector<double>,std::vector<size_t>),
                                 std::vector<double> nodes,std::vector<double> weights, size_t d, double gamma){
    double sum = 0.0; double summand;
    size_t N = nodes.size();
    size_t level = (size_t) std::log2((double) N);
    std::vector<size_t> k(d, 0);
    while(true){
        summand = 1.0;
        for(size_t i = 0; i < d; i++){
            summand*=weights[k[i]];
        }
        summand *= func(gamma, nodes,k);
        sum += summand;
        for(size_t j = 0; j<d; j++){
            k[j] += 1;
            if(k[j] > level-1){
                if(j == d-1) return sum;
                k[j] = 0;
            }
            else break;
        }
    }
}*/

/*
* @param level
* @param d dimension
* @param nodes
* @param weights
* @param nodes1d - nodes of the 1-dimensional qudrature rule
* @param weights1d - weights of the 1-dimensional quadrature rule
*/

void tensor_product_quadrature(size_t level, size_t d, std::vector< std::vector<double> > & nodes, std::vector<double> & weights,
                               const std::vector<double> & nodes1d, const std::vector<double> & weights1d){
    double factor = 1.;
    std::vector<size_t> k(d, 1.);
    size_t N = std::pow(2., level) - 1;
    std::vector<double> nodes_tmp;

    while(true){
        nodes_tmp.reserve(d);
        //computing next weight and node
        factor = 1.;
        for(size_t i = 0; i < d; i++){
            factor *= weights1d[k[i]-1];
            nodes_tmp.push_back(nodes1d[k[i]-1]);
        }
        nodes.push_back(nodes_tmp);
        weights.push_back(factor);

        //for (auto it = k.begin(); it != k.end() ; it ++)
        //    std:: cout << *it << ' ';
        //std::cout << std::endl;
        nodes_tmp.clear();
        for(size_t j = 0; j<d; j++){
            k[j] ++;
            if(k[j] > N){
                if(j == d-1) return;
                k[j] = 1;
            }
            else break;
        }
    }
}


double tensor_product_quadrature(size_t level, size_t d, const std::vector<double> & nodes1d, const std::vector<double> & weights1d,
                                 std::function<double (std::vector<double>) > func){
    double factor = 1.;
    std::vector<size_t> k(d, 1.);
    size_t N = std::pow(2., level) - 1;
    std::vector<double> nodes_tmp;
    double res = 0.;
    while(true){
        nodes_tmp.reserve(d);
        //computing next weight and node
        factor = 1.;
        for(size_t i = 0; i < d; i++){
            factor *= weights1d[k[i]-1];
            nodes_tmp.push_back(nodes1d[k[i]-1]);
        }
        res += factor * func(nodes_tmp);

        //for (auto it = k.begin(); it != k.end() ; it ++)
        //    std:: cout << *it << ' ';
        //std::cout << std::endl;
        nodes_tmp.clear();
        for(size_t j = 0; j<d; j++){
            k[j] ++;
            if(k[j] > N){
                if(j == d-1) return res;
                k[j] = 1;
            }
            else break;
        }
    }
}

/*
* test function
*/
double linear_function(std::vector<double> values){
    return std::accumulate(values.begin(), values.end(), 0.);
}

/*
* Test the product grid approach
*/
void sheet3_task8(){
    size_t d = 3;
    double exact_value = 1.;
    size_t l = 2;
    double tmp = 0.0;
    std::vector<double> nodes1d, weights1d;
    trapezoidal_rule(l, nodes1d, weights1d);

    std::vector< std::vector<double> > nodes;
    std::vector<double> weights;

    tensor_product_quadrature(l, d, nodes, weights, nodes1d, weights1d);
    auto it =  nodes.begin();
    for (size_t i = 0; i < weights.size() ; i++) {
        //tmp += weights[i]*1.; //constant_function(nodes[i]);
        tmp += weights[i]*linear_function(*it); //linear_function(nodes[i]);
        it++;
    }

    std:: cout << "exact value: " << exact_value << '\n';
    std::cout << "approximated value: "<< tmp << '\n';
    tmp = std::abs((exact_value - tmp)/exact_value);
    std::cout << "error: "<< tmp << '\n';

}

// enable doxygen processing for this header:
/** @file */
