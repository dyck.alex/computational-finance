/*!
 *  \date      9. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>
#include <sstream>
#include <list>

/**
 * Testfunction for quadrature rules
 * @param gamma real valued parameter
 * @param values values to multiply
 * @return returns the product of the functionvalues
 */
double test_function(double gamma, std::vector<double> values){
    size_t d = values.size();
    double product = 1.0;
    for(size_t i=0; i < d; i++){
        product *= (1.0 + gamma * std::exp(values[i] * 0.5));
    }
    return product;
}

/**
 * Monte Carlo quadrature rule for multivariate integration
 * @param l Level
 * @param d dimension
 * @param nodes nodes from discretization
 */
void monte_carlo_rule_multidimensional(size_t l, size_t d, std::vector< std::vector<double> > & nodes) {
    gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(r,time(NULL));

    size_t N = pow(2,l)-1;
    nodes.resize(N);
    for(size_t i=0;  i<N; i++){
	    nodes[i].resize(d);
    }

    for(size_t i = 0; i < d; i++){
        for(size_t j = 0; j < N; j++){
            nodes[j][i] = randomNumber01GSL(r);
        }
    }
}

void sheet3_task13(){
    double gamma = 0.1;
    double exact_value;
    size_t d;

    std::ofstream f;
    for(size_t j = 0; j < 4; j++){
        d = (size_t) std::pow(2.,j);
        exact_value = std::pow(1+ 2*gamma*exp(0.5) - 2*gamma,d);

        std::stringstream ss;
        ss << j ;
        f.open("s3t13_"+ss.str()+".dat");

        for (size_t l = 2; l<7; l++) {
            if (d==8 && l>3) break;
            std::cout << "d " << d << " level " << l << '\n';
            double tmp_mc = 0 , tmp_qmc = 0, tmp_t_pg = 0, tmp_cc_pg = 0 , tmp_t_sg = 0, tmp_cc_sg = 0;
            size_t N = pow(2,l) - 1;
            std::vector< std::vector<double> > nodes;
	        std::vector<double> weights;

            //Monte Carlo
            monte_carlo_rule_multidimensional(l, d, nodes);
            for (size_t i = 0; i < N ; i++) {
                tmp_mc += test_function(gamma, nodes[i]);
            }
            tmp_mc /= N;
            tmp_mc = std::abs((exact_value - tmp_mc)/exact_value);
            nodes.clear();

            //Quasi Monte Carlo
            halton(nodes, d, N);
            for (size_t i = 0; i < N ; i++) {
                tmp_qmc += test_function(gamma, nodes[i]);
            }
            tmp_qmc /= N;
            tmp_qmc = std::abs((exact_value - tmp_qmc)/exact_value);
            nodes.clear();
            weights.clear();

            //Trapezoidal rule
            //product grid
            std::vector<double> nodes1d, weights1d;
            trapezoidal_rule(l, nodes1d, weights1d);
            /*
    	    tensor_product_quadrature(l, d, nodes, weights, nodes1d, weights1d);
            //std::cout << nodes.size() << ' ' << weights.size() << std::endl;
            for (size_t i = 0; i < weights.size() ; i++) {
                tmp_t_pg += weights[i] * test_function(gamma, nodes[i]);
            }
            //std::cout << tmp_t_pg << std::endl;
	        tmp_t_pg = std::abs((exact_value - tmp_t_pg)/exact_value);
            */
            tmp_t_pg = tensor_product_quadrature(l,d,nodes1d,weights1d,
                                                  [&](std::vector<double> x)->double {return test_function(gamma, x);});
            tmp_t_pg = std::abs((exact_value - tmp_t_pg)/exact_value);
            nodes1d.clear() , weights1d.clear();
            nodes.clear();
            weights.clear();

            //sparse grid
            std::vector< std::vector<double> > sparse_nodes;
            std::vector<double> sparse_weights;

            sparse_grid_quadrature(l, d, sparse_nodes, sparse_weights, 0);
            auto it_nodes = sparse_nodes.begin();
            for (auto it = sparse_weights.begin(); it != sparse_weights.end(); ++it) {
                tmp_t_sg += (*it)*test_function(gamma, *it_nodes);
                it_nodes++;
            }
            tmp_t_sg = std::abs((exact_value - tmp_t_sg)/exact_value);
            nodes1d.clear() , weights1d.clear();
            sparse_nodes.clear();
            sparse_weights.clear();
            nodes.clear();
            weights.clear();
            //Clenshaw Curtis rule
            //product grid
            /*
            tensor_product_quadrature(l, d, nodes, weights, nodes1d, weights1d);
            for (size_t i = 0; i < weights.size() ; i++) {
                tmp_cc_pg += weights[i]*test_function(gamma, nodes[i]);
            }
            tmp_cc_pg = std::abs((exact_value - tmp_cc_pg)/exact_value);
            */
            clenshaw_curtis_rule(l,nodes1d,weights1d);
            tmp_cc_pg = tensor_product_quadrature(l,d,nodes1d,weights1d,
                                                  [&](std::vector<double> x) {return test_function(gamma, x);});
            tmp_cc_pg = std::abs((exact_value - tmp_cc_pg)/exact_value);
            //sparse grid
            sparse_nodes.clear();
            sparse_weights.clear();

            sparse_grid_quadrature(l, d, sparse_nodes, sparse_weights,1);
            it_nodes = sparse_nodes.begin();
            for (auto it = sparse_weights.begin(); it != sparse_weights.end(); ++it){
                tmp_cc_sg += (*it)*test_function(gamma, *it_nodes);
                it_nodes++;
            }
            tmp_cc_sg = std::abs((exact_value - tmp_cc_sg)/exact_value);

            sparse_nodes.clear();
            sparse_weights.clear();

            f << N << ' ' << tmp_mc << ' ' << tmp_qmc << ' ' << tmp_t_pg << ' ' << tmp_cc_pg << ' ' << tmp_t_sg << ' ' << tmp_cc_sg <<'\n';
        }
	    f.close();
    }
}

// enable doxygen processing for this header:
/** @file */
