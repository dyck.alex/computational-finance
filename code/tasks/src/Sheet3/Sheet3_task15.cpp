/*!
 *  \date      8. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <gsl/gsl_rng.h>
#include <cmath>
#include <iostream>
#include <sstream>
/**
 * function for integrandd in multidimensional computation
 * @param time
 * @param process
 * @param S0
 * @param r
 * @param sig
 * @param K
 * @return
 */
double integrand_discrete_geometric_average(const std::vector <double> & time, const std::vector <double> & process, double S0, double r , double sig, double K ) {
    std::vector <double> pro (process.size());
    for (size_t k = 0 ; k< process.size(); k++) {
        pro[k] = std::sqrt(time[k]) * NormalInverseCDF(process[k]);
    }
    geometric_brownian_motion(time, pro, S0, r, sig);
    return Asian::discrete_geometric_average(pro,K);
}

void sheet3_task15() {
    gsl_rng* ran = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(ran,time(NULL));

    double S0 = 10 , r = 0.1 , sig = 0.25 , T=1 , M= 16 , K =0.;
    std::list< std::vector<double> > nodes;
    std::vector <double>  weights;

    size_t d = M;
    std::ofstream f;
    f.open("s3t15.dat");

    for(size_t l = 1; l < 7; l++){
        std::cout << l << std::endl;
        d=M;
        double exact_value = Asian::Vcall_discrete(S0,r,sig,T,K,T/M,M)*std::exp(r*T),
        tmp_rw =0 , tmp_br = 0;
	    //std::cout << "exact " << exact_value << '\n';

        std::vector<double> time_RW, time_BR;

        create_equidistant_timesteps(time_RW, T, T/M);

        time_generate(time_BR,T,M);
        //sparse grid
        std::vector<double> nodes1d, weights1d;
        std::vector< std::vector<double> > sparse_nodes;
        std::vector<double> sparse_weights;
        clenshaw_curtis_rule(l,nodes1d,weights1d);
        sparse_nodes.clear();
        sparse_nodes.clear();
        sparse_grid_quadrature(l, d, sparse_nodes, sparse_weights,1);

        auto it_nodes = sparse_nodes.begin();
        for (auto it = sparse_weights.begin(); it != sparse_weights.end(); ++it) {
            tmp_rw += (*it)*integrand_discrete_geometric_average(time_RW,*it_nodes,S0,r,sig,K);
            tmp_br += (*it)*integrand_discrete_geometric_average(time_BR,*it_nodes,S0,r,sig,K);
            it_nodes++;
        }
        f << std::pow(2.,l)-1 << ' ' << std::abs((exact_value - tmp_rw)/exact_value) << ' ' << std::abs((exact_value - tmp_br)/exact_value) <<'\n';
    }
    f.close();
}



// enable doxygen processing for this header:
/** @file */
