/*!
 *  \date      9. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>

void sheet3_task11(){
    std::vector<double> nodes1, weights1;
    std::vector<double> nodes2, weights2;
    size_t level1 = 5;
    size_t level2 = 7;
    trapezoidal_rule(level1, nodes1, weights1);
    clenshaw_curtis_rule(level1,nodes2,weights2);

    std::ofstream f, f2;
    f.open("s3t11_1.dat");
    f2.open("s3t11_2.dat");
    std::vector<size_t> k(2,1);
    size_t S = 2;
    size_t stopp = 0;
    while(stopp == 0) {
        for (size_t i = 0; i < (size_t)(std::pow(2, k[0]) - 1); i++) {
            for (size_t j = 0; j < (size_t)(std::pow(2, k[1]) - 1); j++) {
                f << nodes1[(int) std::pow(2, level1 - k[0]) * (i + 1) - 1] << " "
                  << nodes1[(int) std::pow(2, level1 - k[1]) * (j + 1) - 1] << '\n';
                f2 << nodes2[(int) std::pow(2, level1 - k[0]) * (i + 1) - 1] << " "
                   << nodes2[(int) std::pow(2, level1 - k[1]) * (j + 1) - 1] << '\n';
            }
            for (size_t j = 0; j < 2; j++) {
                k[j] += 1;
                S += 1;
                if (S > 1 + level1) {
                    if (j == 1) stopp = 1;
                    S -= (k[j] - 1);
                    k[j] = 1;
                } else break;
            }
        }
    }
    f.close();
    f2.close();
    trapezoidal_rule(level2, nodes1, weights1);
    clenshaw_curtis_rule(level2,nodes2,weights2);

    f.open("s3t11_3.dat");
    f2.open("s3t11_4.dat");
    k[0] = 1; k[1] = 1;
    S = 2;
    stopp = 0;
    while(stopp == 0){
        for(size_t i=0; i<(size_t)(std::pow(2,k[0])-1); i++){
            for(size_t j=0; j<(size_t)(std::pow(2,k[1])-1); j++){
                f << nodes1[(int)std::pow(2,level2-k[0])*(i+1)-1] << " " << nodes1[(int)std::pow(2,level2-k[1])*(j+1)-1] << '\n';
                f2 << nodes2[(int)std::pow(2,level2-k[0])*(i+1)-1] << " " << nodes2[(int)std::pow(2,level2-k[1])*(j+1)-1] << '\n';
            }
        }
        for(size_t j=0; j<2; j++){
            k[j]+=1;
            S+=1;
            if(S > 1 + level2){
                if(j == 1) stopp = 1;
                S -= (k[j] - 1);
                k[j] = 1;
           }
            else break;
    	}
   }
   f.close();
   f2.close();
}
// enable doxygen processing for this header:
/** @file */
