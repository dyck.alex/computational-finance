/*!
 *  \date      6. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>

/**
 *
 */
namespace Asian {
    /**
     * @param S0 Starting point
     * @param r risk
     * @param sigma variance
     * @param T End time
     * @param K Strike price
     * @param timestep
     * @param M number of time steps
     * @return fair price depending on multiple time steps using the closed form for the discrete geometric average
     */
    double Vcall_discrete(double S0, double r, double sigma, double T, double K, double timestep, size_t M) {
        double T1 = T - (M*(M-1)*(4.0*M +1)*timestep)/(6.*M*M);
        double T2 = T - (M-1)*timestep*0.5;
        double A = std::exp(-r*(T-T2) - sigma*sigma*(T2-T1)*0.5);
        double d = (std::log(S0/K) + (r- sigma*sigma*0.5 )*T2)/(sigma*std::sqrt(T1));

        return S0 * A * NormalCDF(d +sigma * std::sqrt(T1))  - K* std::exp(-r*T)*NormalCDF(d);
    }
    /**
     * @param S0 Starting point
     * @param r risk
     * @param sigma variance
     * @param T End time
     * @param K Strike price
     * @return fair price depending on multiple time steps using the closed form for the continuuous geometric average
     */
    double Vcall_continuous(double S0, double r, double sigma, double T, double K) {
        double d = (log(S0/K) + 0.5*(r - sigma*sigma/2. ) * T)/(sigma * std::sqrt(T/3.));
        return S0 * std::exp(-0.5*(r + sigma * sigma/6.) * T) * NormalCDF(d+sigma * std::sqrt(T/3.)) - K*std::exp(-r*T)*NormalCDF(d);
    }
}

// enable doxygen processing for this header:
/** @file */
