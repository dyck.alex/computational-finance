/*!
 *  \date      9. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>
#include <sstream>
#include <list>

void sheet3_task16(){
    gsl_rng* ran = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(ran,time(NULL));

    double S0 = 10 , r = 0.1 , sig = 0.25 , T=1 , M= 8 , K =0.;

    size_t d = M;
    std::ofstream f, g;
    f.open("s3t16_RW.dat");
    g.open("s3t16_BR.dat");
    for(size_t l = 1; l < 10; l++){
        std::cout << "l = " <<  l << std::endl;
        double exact_value = Asian::Vcall_discrete(S0,r,sig,T,K,T/M,M)*std::exp(r*T),
                mc_rw =0 , mc_br = 0,
                qmc_rw =0 , qmc_br = 0,
                cc_sg_rw =0 , cc_sg_br = 0,
                cc_pg_rw =0 , cc_pg_br = 0,
                t_sg_rw =0 , t_sg_br = 0,
                t_pg_rw =0 , t_pg_br = 0;

        std::vector<double> time_RW, time_BR, BM_RW, BM_BR, time_help;
        size_t N = std::pow(2.,l);
        create_equidistant_timesteps(time_RW, T, T/M);
        time_generate(time_BR,T,M);

        std::vector<double> nodes1d, weights1d, process, BR;
        process.resize(M);
        std::vector< std::vector<double> > nodes, nodes2;
        std::vector<double> weights;
        /*
        //Monte Carlo
        monte_carlo_rule_multidimensional(l, d, nodes);
        nodes2 = nodes;
        for (size_t i = 0; i < N ; i++) {
            random_walk(time_RW, nodes[i]);
            create_GBM(process, S0, r, sig, nodes[i], time_RW);
            mc_rw += Asian::discrete_geometric_average(process,K);

            time_help = time_BR;
            WienerProcess_BR(nodes2[i], time_BR, BR, T, M);
            nodes2[i] = BR;
            create_GBM(process, S0, r, sig, nodes2[i], time_BR);

            mc_br += Asian::discrete_geometric_average(process,K);
            time_BR = time_help;
        }
        mc_rw /= N*std::exp(r*T);
        mc_br /= N*std::exp(r*T);
        */
        //Quasi Monte Carlo
        halton(nodes, d, N);
        nodes2 = nodes;
        for (size_t i = 0; i < N ; i++) {
            random_walk(time_RW, nodes[i]);
            create_GBM(process, S0, r, sig, nodes[i], time_RW);
            qmc_rw += Asian::discrete_geometric_average(process,K);

            time_help = time_BR;
            WienerProcess_BR(nodes2[i], time_BR, BR, T, M);
            nodes2[i] = BR;
            create_GBM(process, S0, r, sig, nodes2[i], time_BR);
            qmc_br += Asian::discrete_geometric_average(process,K);
            time_BR = time_help;
        }
        qmc_rw /= N*std::exp(r*T);
        qmc_br /= N*std::exp(r*T);


        // Product grid
        //trapezoidal_rule(l,nodes1d,weights1d);
        //t_pg_rw = tensor_product_quadrature(l,d,nodes1d,weights1d, [&] (std::vector<double> x)->double { return integrand_discrete_geometric_average(time_RW,x ,S0,r,sig,K); });
        //t_pg_br = tensor_product_quadrature(l,d,nodes1d,weights1d, [&] (std::vector<double> x)->double { return integrand_discrete_geometric_average(time_BR,x ,S0,r,sig,K); });
        nodes1d.clear();
        weights1d.clear();

        clenshaw_curtis_rule(l,nodes1d,weights1d);
        //cc_pg_rw = tensor_product_quadrature(l,d,nodes1d,weights1d, [&] (std::vector<double> x)->double { return integrand_discrete_geometric_average(time_RW,x ,S0,r,sig,K); });
        //cc_pg_br = tensor_product_quadrature(l,d,nodes1d,weights1d, [&] (std::vector<double> x)->double { return integrand_discrete_geometric_average(time_BR,x ,S0,r,sig,K); });

        //sparse grid

        t_sg_rw = sparse_grid_quadrature(l, d, [&] (std::vector<double> x)->double { return integrand_discrete_geometric_average(time_RW,x ,S0,r,sig,K); },0);
        t_sg_br = sparse_grid_quadrature(l, d, [&] (std::vector<double> x)->double { return integrand_discrete_geometric_average(time_BR,x ,S0,r,sig,K); },0);

        cc_sg_rw = sparse_grid_quadrature(l, d, [&] (std::vector<double> x)->double { return integrand_discrete_geometric_average(time_RW,x ,S0,r,sig,K); },1);
        cc_sg_br = sparse_grid_quadrature(l, d, [&] (std::vector<double> x)->double { return integrand_discrete_geometric_average(time_BR,x ,S0,r,sig,K); },1);

        cc_pg_rw = std::abs((exact_value - cc_pg_rw)/exact_value);
        cc_sg_rw = std::abs((exact_value - cc_sg_rw)/exact_value);
        t_pg_rw = std::abs((exact_value - t_pg_rw)/exact_value);
        t_sg_rw = std::abs((exact_value - t_sg_rw)/exact_value);
        mc_rw = std::abs((exact_value - mc_rw)/exact_value);
        qmc_rw = std::abs((exact_value - qmc_rw)/exact_value);

        cc_pg_br = std::abs((exact_value - cc_pg_br)/exact_value);
        cc_sg_br = std::abs((exact_value - cc_sg_br)/exact_value);
        t_pg_br = std::abs((exact_value - t_pg_br)/exact_value);
        t_sg_br = std::abs((exact_value - t_sg_br)/exact_value);
        mc_br = std::abs((exact_value - mc_br)/exact_value);
        qmc_br = std::abs((exact_value - qmc_br)/exact_value);

        f << std::pow(2.,l)-1 << ' ' << mc_rw << ' ' << qmc_rw << ' ' << t_pg_rw << ' ' << t_sg_rw << ' '  << cc_pg_rw << ' ' << cc_sg_rw << ' '  << '\n';
        g << std::pow(2.,l)-1 << ' ' << mc_br << ' ' << qmc_br << ' ' << t_pg_br << ' ' << t_sg_br << ' '  << cc_pg_br << ' ' << cc_sg_br << ' '  << '\n';
    }
    f.close();
    g.close();
}

// enable doxygen processing for this header:
/** @file */
