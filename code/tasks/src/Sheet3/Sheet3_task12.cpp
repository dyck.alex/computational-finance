/*!
 *  \date      9. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>

void sheet3_task12(){
   size_t level = 4;

   std::ofstream f;
   f.open("s3t12.dat");

   //sparse grid
   size_t S, sum, sum2, stopp, stopp2, d, help;
   for(d = 1; d < 11; d++){
	sum = 0;
	stopp = 0; S = d;
	std::vector<size_t> k(d,1);
   	while(stopp == 0){
   		help = 1;
   		for(size_t i = 0; i < d; i++){
   			help *= std::pow(2,k[i] - 1);
   		}
		sum += help;
		for(size_t j = 0; j < d; j++){
	   	k[j] += 1;
	   	S += 1;
	   	if(S > d + level - 1){
			if(j == d - 1) stopp = 1;
			S -= (k[j]-1);
			k[j] = 1;
           	}
	   	else break;
		}
   	}
	//tensor product
	sum2 = 0; stopp2 = 0;
	std::vector<size_t> l(d,1);
    while(stopp2 == 0){
       help = 1;
       for(size_t i =0; i<d; i++){
       		help*=std::pow(2,l[i]-1);
       }
	   sum2+=help;
	   for(size_t j=0; j<d; j++){
	   	l[j]+=1;
		if(l[j]>level){
		   if(j==d-1) stopp2=1;
		   l[j]=1;
		}
		else break;
	   }
        }
	f << d << " " << sum << " " << sum2 << '\n';
   }
   f.close();
}

// enable doxygen processing for this header:
/** @file */
