#include "../tasks.h"
#include <gsl/gsl_rng.h>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <numeric>

/*
 * time generate works, but the appropriate wiener process had
 * some bugs. A more beautiful way has to be found. Next time.
 */

/**
 * generates the time values for the Brownian bridge construction
 * @param time where times are saved
 * @param T maximal deflection of time
 * @param M number of time steps
 */
void time_generate(std::vector <double> & time, double T, size_t M) {
	time.resize(M+1);
	time[0] = 0;
	time[1] = T;
	
	size_t j = 1;
	size_t k = 2;
	
	for (size_t i = 2; i < M+1; i++) {
		time[i] = (double)j*T/k;
		if (k == j+1) {
			j = 1;
			k*=2; //k += 2;
		}
		else j += 2;
	}
}

/**
 * Find the index in the time vector for a specific time point
 * @param point time point
 * @param time time values
 */
int find_index(double point, std::vector <double> & time){
    for(size_t index=0; index < time.size(); index++){
	    if(time[index]==point) return index;
    }
    std::cout << "No index found!" << '\n';
    return -1;
}

std::vector<std::size_t> sort_permutation(
        const std::vector<double>& vec)
{
    std::vector<std::size_t> p(vec.size());
    std::iota(p.begin(), p.end(), 0);
    std::sort(p.begin(), p.end(),
              [&](std::size_t i, std::size_t j){ return vec[i] < vec[j]; });
    return p;
}


std::vector<double> apply_permutation(
        const std::vector<double>& vec,
        const std::vector<std::size_t>& p)
{
    std::vector<double> sorted_vec(vec.size());
    std::transform(p.begin(), p.end(), sorted_vec.begin(),
                   [&](std::size_t i){ return vec[i]; });
    return sorted_vec;
}

/**
 * Brownian brigde construction
 * @param r random number generator
 * @param time time values
 * @param BR brownian bridge
 * @param T max time deflection
 * @param M number of timesteps
 */

void WienerProcess_BR(std::vector <double> nodes, std::vector <double> & time, std::vector<double> & BR, double T, size_t M) {
    BR.resize(time.size());

    BR[0] = 0;
    BR[1] = std::sqrt(T)*NormalInverseCDF(nodes[0]);
    for(size_t level=1; level <= (size_t) std::log2(M); level++){
        double point = std::pow(2.,-(double)level)*T;
        double a = std::pow(2.,-(double)level)*T;
        while(point < T){
            size_t index = find_index(point, time);
            if(BR[index]==0){
                size_t preceding_index = find_index(point-a, time);
                size_t subsequent_index = find_index(point+a, time);
                double rnd = NormalInverseCDF(nodes[index-1]);
                BR[index] = (BR[preceding_index] + BR[subsequent_index])/2.+std::sqrt(a/2.)*rnd;
            }
            point+=std::pow(2.,-(double)level)*T;
        }
    }
    std::vector < size_t> permutation = sort_permutation(time);
    time = apply_permutation(time,permutation);
    BR = apply_permutation(BR, permutation);
    BR.erase(BR.begin());
}



void sheet3_task14(){

    //gsl_rng* ran = gsl_rng_alloc(gsl_rng_mt19937);
    //gsl_rng_set(ran,time(NULL));
    //std::vector<double> time, BR;
    //double T = 1;
    //size_t M = 16;
    //WienerProcess_BR(ran,time, BR, T, M);

}


// enable doxygen processing for this header:
/** @file */
