/*!
 *  \date      8. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>

/**
 * @param sequence to save sequence
 * @param p prime number
 * @param n length f sequence
 */
void van_der_corput(std::vector<double> & sequence, size_t p, size_t n){
    sequence.resize(n+1);
    double eps = 1e-12;
    sequence[0] = 0.0;
    for (size_t i=1; i<n+1; i++) {
        double z = 1. - sequence[i-1];
        double v = 1./p;
        while(z - v < eps){
             v = v/p;
        }
        sequence[i] = (sequence[i-1] + (p+1)*v) - 1.;
        if (sequence[i]< 0 )
            sequence[i] = 0;
    }
}

/**
 * Prime number generator. Fills sequence with d prime numbers
 * @param sequence where prime numbers are saved
 * @param d number of prime numbers
 */
void prime_numbers(std::vector<size_t> & sequence, size_t d){
     sequence.resize(d);
     bool isPrime;
     size_t k = 0;
     size_t p = 2;
     while(k<d){
        isPrime = false;
        for(size_t j = 2; j <= p/2.; j++){
            if(p % j == 0){
                isPrime = true;
                p++;
                break;
            }
        }
        if(isPrime==0){
            sequence[k] = p;
            k++; p++;
	    }
    }
}

/**
 * Generates Halton sequence
 * @param sequence array to saved in
 * @param d number of prime numbers
 * @param n length of Halton sequence
 */
void halton(std::vector< std::vector<double> > & sequence, size_t d, size_t n){
    sequence.resize(n);
    std::vector<double> van_corput;
    std::vector<size_t> prime;
    prime_numbers(prime, d);
    for(size_t i=0;  i<n; i++){
    	sequence[i].resize(d);
    }
    for(size_t i = 0; i < d; i++){
	    van_der_corput(van_corput, prime[i], n);
        for(size_t j = 0; j < n; j++){
            sequence[j][i] = van_corput[j+1];
        }
    }
}

/**
 * Main function to execute task 6
 */
void sheet3_task6(){
    size_t d = 10;
    size_t n = 1;
    std::vector< std::vector<double> > sequence;
    halton(sequence,d,n);
    for(size_t i = 0; i < n; i++){
	for(size_t j = 0; j < d; j++){
	    std::cout << sequence[i][j] << " ";
	}
	std::cout << '\n';
   }
}

// enable doxygen processing for this header:
/** @file */
