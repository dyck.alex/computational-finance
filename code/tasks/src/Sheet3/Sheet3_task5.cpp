/*!
 *  \date      8. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>

namespace Asian {
    /**
     * @param process
     * @param K strike price
     * @return discrete artihmetic average
     */
    double discrete_arithmetic_average(const std::vector<double> & process, double K){
        size_t M = process.size();
        return std::max(std::accumulate(process.begin(),process.end(), 0.0)/M-K, 0.0);
    }
}

/**
 *
 * @param S0 Starting point
 * @param r risk factor
 * @param sigma variance
 * @param K strike price
 * @param s1 first integral variable
 * @param s2 second integral variable
 * @return integrand in two dimensions
 */
double integrand_2d(double S0, double r, double sigma, double K, double s1, double s2){
    double t1 = 0.5;
    double t2 = 1.0;
    std::vector<double> process(2);
    process[0] = S0*std::exp((r-0.5*sigma*sigma)*t1+sigma*std::sqrt(t1)*NormalInverseCDF(s1));
    process[1] = S0*std::exp((r-0.5*sigma*sigma)*t2+sigma*std::sqrt(t2)*NormalInverseCDF(s2));	
    return Asian::discrete_arithmetic_average(process, K);
}

void sheet3_task5(){
   double S0 = 10 , r = 0.1 , sigma = 0.25 , K = 10;
   std::ofstream f;
   f.open("s3t5.dat");
   for(double i = 1.0/50.0; i < 1.0; i+=1.0/50.0){
	for(double j = 1.0/50.0; j < 1.0; j+=1.0/50.0){
		f << i << ' ' << j << ' ' << integrand_2d(S0, r, sigma, K, i, j) << '\n';
	}
   }
   f.close();
}
// enable doxygen processing for this header:
/** @file */
