/*!
 *  \date      8. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <list>
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>
#include <tuple>

/*
* @param level
* @param d dimension
* @param nodes
* @param weights
* @param nodes1d - nodes of the 1-dimensional qudrature rule
* @param weights1d - weights of the 1-dimensional quadrature rule
* @param rule 0 - trapezoidal, 1 - clenshaw
*/
void sparse_grid_quadrature(size_t level, size_t d,
                            std::vector<std::vector<double> > & nodes,
                            std::vector<double> & weights, int rule){
    std::vector<size_t> k(d,1);
    nodes.reserve(level*level);
    size_t S = d;
    //compute deltas
    std::vector< std::vector<double > > delta_nodes(level), delta_weights(level);
    std::vector<double> nodes_tmp, weights_tmp;

    for (size_t l = 0; l < level; l++) {
        if(rule == 0)
            trapezoidal_rule(l+1, delta_nodes[l], delta_weights[l]);
        else clenshaw_curtis_rule(l+1, delta_nodes[l], delta_weights[l]);
    }
    for (size_t l = level; l > 1; l--) {
        size_t N = std::pow(2.,l-1)-1;
        for (size_t m = 0; m < N; m++ )
            delta_weights[l-1][m*2+1] -= delta_weights[l-2][m];
    }


    while(true) {
        std::vector<size_t> k_inner(d,0);
        bool stop = 0;
        while(stop==0) {
            //compute next weight and node
            double factor = 1.0;
            std::vector<double> helping_vector;
            helping_vector.reserve(d*level);
            for (size_t i = 0; i < d; i++) {
                factor *= delta_weights[k[i] - 1][k_inner[i]];
                helping_vector.push_back(delta_nodes[k[i] - 1][k_inner[i]]);
            }
            nodes.push_back(helping_vector);
            weights.push_back(factor);
            for (size_t inner_j = 0; inner_j < d; inner_j++) {
                k_inner[inner_j] += 1;
                if (k_inner[inner_j] > (size_t) std::pow(2.0, k[inner_j]) - 2) {
                    if (inner_j == d - 1) stop = 1;
                    k_inner[inner_j] = 0;
                } else break;
            }
        }

        //next level combination
        for(size_t j = 0; j < d; j++){
            k[j]++;
            S ++;
            if (S > d + level - 1){
                if(j == d-1) return;
                S -= (k[j]-1);
                k[j] = 1;
            }
            else break;
        }
    }
}

double sparse_grid_quadrature(size_t level, size_t d, std::function<double (std::vector<double> ) > func, int rule){
    std::vector<size_t> k(d,1);
    size_t S = d;
    //compute deltas
    std::vector< std::vector<double > > delta_nodes(level), delta_weights(level);
    std::vector<double> nodes_tmp, weights_tmp;

    for (size_t l = 0; l < level; l++) {
        if(rule == 0)
            trapezoidal_rule(l+1, delta_nodes[l], delta_weights[l]);
        else clenshaw_curtis_rule(l+1, delta_nodes[l], delta_weights[l]);
    }
    for (size_t l = level; l > 1; l--) {
        size_t N = std::pow(2.,l-1)-1;
        for (size_t m = 0; m < N; m++ )
            delta_weights[l-1][m*2+1] -= delta_weights[l-2][m];
    }

    double res =0.;
    while(true) {
        std::vector<size_t> k_inner(d,0);
        bool stop = 0;
        while(stop==0) {
            //compute next weight and node
            double factor = 1.0;
            std::vector<double> helping_vector;
            helping_vector.reserve(d*level);
            for (size_t i = 0; i < d; i++) {
                factor *= delta_weights[k[i] - 1][k_inner[i]];
                helping_vector.push_back(delta_nodes[k[i] - 1][k_inner[i]]);
            }
            res += factor * func(helping_vector);

            for (size_t inner_j = 0; inner_j < d; inner_j++) {
                k_inner[inner_j] += 1;
                if (k_inner[inner_j] > (size_t) std::pow(2.0, k[inner_j]) - 2) {
                    if (inner_j == d - 1) stop = 1;
                    k_inner[inner_j] = 0;
                } else break;
            }
        }

        //next level combination
        for(size_t j = 0; j < d; j++){
            k[j]++;
            S ++;
            if (S > d + level - 1){
                if(j == d-1) return res;
                S -= (k[j]-1);
                k[j] = 1;
            }
            else break;
        }
    }
}

/*
* Test the sparse grid approach
*/
double f (std::vector<double> x) {
    return std::accumulate(x.begin(),x.end(),0.);
}

void sheet3_task10() {
    double exact_value = 1.0;
    size_t d = 2;
    size_t l = 2;
    double tmp = 0.0;

    std::vector< std::vector<double> > sparse_nodes;
    std::vector<double> sparse_weights;

    sparse_grid_quadrature(l,d,sparse_nodes, sparse_weights,0);
    auto it_nodes = sparse_nodes.begin();
    for (auto it=sparse_weights.begin(); it != sparse_weights.end(); ++it) {
        tmp += (*it)*f(*it_nodes);
        it_nodes++;
    }
    std:: cout << "exact value: " << exact_value << '\n';
    std::cout << "approximated value: "<< tmp << '\n';
    tmp = std::abs((exact_value - tmp)/exact_value);
    std::cout << "error: "<< tmp << '\n';

}


// enable doxygen processing for this header:
/** @file */
