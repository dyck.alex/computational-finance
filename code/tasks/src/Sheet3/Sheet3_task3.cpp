/*!
 *  \date      29. May 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>

namespace Asian {
    /**
     * Computes the discrete geometric average in a plain and simple way
     * @param process
     * @param K Strike price
     * @return the discrete geometric average
     */
    double discrete_geometric_average(const std::vector<double> & process, double K) {
        size_t M = process.size();//-1;
        return std::max(std::pow(std::accumulate(process.begin(), process.end(), 1.,std::multiplies<double>()), 1./M) -K,
                        0.0);
    }
}

void sheet3_task3() {
    gsl_rng* ran = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(ran,time(NULL));

    double S0 = 10 , r = 0.1 , sigma = 0.25 , T= 1. , K = 10;

    size_t M = 10+1 , M2 = 200+1 , N = 1000000;
    double ref = Asian::Vcall_discrete(S0,r,sigma , T, K , T/M,M);
    double ref2 = Asian::Vcall_discrete(S0,r,sigma , T, K , T/M2,M2);

    std::vector<double> BM, BM2;
    std::vector<double> time, time2;

    create_equidistant_timesteps(time,T,T/M);
    create_equidistant_timesteps(time2,T,T/M2);

    double mc = 0, mc2 = 0;

    std::ofstream f;
    f.open("s3t3.dat");
    for (size_t i = 1 ; i < N; i*=2) {
        for (size_t j = 0; j < i ; j++) {
            WienerProcess(ran,time,BM);
            WienerProcess(ran,time2,BM2);
            geometric_brownian_motion(time,BM,S0,r,sigma);
            geometric_brownian_motion(time2,BM2,S0,r,sigma);
            mc += Asian::discrete_geometric_average(BM,K)*std::exp(-r*T);
            mc2 += Asian::discrete_geometric_average(BM2,K)*std::exp(-r*T);
        }
        mc /=i , mc2 /=i;
        double err_mc = std::abs(mc-ref)/ref;
        double err_mc2 = std::abs(mc2-ref2)/ref2;
        f << i << ' ' << err_mc << ' ' << err_mc2 << '\n';
        mc = 0 , mc2 = 0;
    }
    f.close();
}



// enable doxygen processing for this header:
/** @file */
