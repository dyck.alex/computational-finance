/*!
 *  \date      8. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>


void sheet3_task7(){
   gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);
   std::ofstream f;
   f.open("s3t6_1.dat");
   for(int i=0; i < 100; i++){
      double u1 = randomNumber01GSL(r), u2 = randomNumber01GSL(r);
      f << u1 << ' ' << u2 << "\n";
   }
   f.close();
   gsl_rng_free(r);
   f.open("s3t6_2.dat");
   std::vector< std::vector<double> > sequence;
   halton(sequence,2,100);

   for(int i=0; i < 100; i++){
        f << sequence[i][0] << ' ' << sequence[i][1] << "\n";
   }
   f.close();
}

// enable doxygen processing for this header:
/** @file */
