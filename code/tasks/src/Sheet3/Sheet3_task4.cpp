/*!
 *  \date      29. May 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>



void sheet3_task4() {
    double S0 = 10 , r = 0.1 , sigma = 0.25 , T= 1. , K = 10;

    size_t M = std::pow(2.,24);
    double ref = Asian::Vcall_continuous(S0,r,sigma , T, K);

    std::ofstream f;
    f.open("s3t4.dat");
    for (size_t i = 1 ; i < M; i*=2) {
        f << i << ' ' << std::abs(Asian::Vcall_discrete(S0, r, sigma, T, K, T/i, i) - ref ) <<'\n';
    }
    f.close();
}



// enable doxygen processing for this header:
/** @file */
