/*!
 *  \date      29. May 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <iostream>
#include <gsl/gsl_integration.h>
#include <vector>
#include <cmath>


/**
* Returns the nodes and weights for the gauss legendre rule
* @param l level
* @param nodes quadrature nodes
* @param weights quadrature weights
*/
void gauss_legendre_rule(int l,std::vector<double> & nodes,std::vector<double> & weights, double a , double b) {
    size_t N = pow(2,l)-1;
    gsl_integration_glfixed_table * table = gsl_integration_glfixed_table_alloc(N);

    nodes.resize(N); weights.resize(N);

    for (size_t i = 0; i< N; i++) {
        gsl_integration_glfixed_point(a,b,i,&nodes[i], &weights[i], table);
    }

    gsl_integration_glfixed_table_free(table);
}


/**
 * evaluates a standard polynomial with coefficients at point x
 * @param coefficients of the monomials
 * @param x
 * @return p(x) = \f$ \sum_{i=0}^{N} a_i x^i \f$
 */
double polynomial(std::vector<double> coefficients, double x){
    double sum = 0;
    for(size_t i=0; i < coefficients.size(); i++) {
	    sum += coefficients[i]*std::pow(x,i);
    }
    return sum;
}

/**
* Main function
*/
void sheet2_task7() {
    srand (time(NULL));
    int l = rand() % 5; 
    size_t N = pow(2,l)-1;
    std::vector<double> nodes, weights;

    gauss_legendre_rule(l,nodes,weights,0.,1.);

    std::vector<double> coefficients(2*N);
    for(size_t i=0; i<coefficients.size(); i++){
        coefficients[i] = rand() % 5;
    }

    double res = 0;
    for(size_t i = 0; i < coefficients.size(); i++){
	    res += coefficients[i]*(1.0/(i+1));
    }
    std::cout << "Right result: " << res << std::endl;

    res = 0;
    for (size_t j = 0; j <N ; j++) {
         res += weights[j] * polynomial(coefficients, nodes[j]);
    }
    std::cout << "Computed result: " << res << std::endl;
}

// enable doxygen processing for this header:
/** @file */
