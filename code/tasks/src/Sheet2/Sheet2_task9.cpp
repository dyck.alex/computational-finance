/*!
 *  \date      22. May 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */

#include "../tasks.h"
#include <iostream>
#include <gsl/gsl_rng.h>
#include <vector>
#include <cmath>

/**
 * evaluation of the function \f$ 1+ \gamma \exp(\frac{x}{2})\f$
 * @param gamma
 * @param x
 * @return \f$ f(x) = 1+ \gamma \exp(\frac{x}{2})\f$
 */
double f_gamma(double gamma, double x) {
    return 1 + gamma* exp(x/2.);
}

/**
* Returns the nodes for the monte-carlo rule
* @param l level
* @param nodes quadrature nodes
*/
void monte_carlo_rule(int l,std::vector<double> & nodes) {
    gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(r,time(NULL));

    size_t N = pow(2,l)-1;
    nodes.resize(N);

    for (auto it = nodes.begin(); it != nodes.end(); it++)
        *it = randomNumber01GSL(r);
}

/**
* Main function
*/
void sheet2_task9() {
    double gamma = 1., exact_value = 2*exp(1./2)-1;

    std::ofstream f;
    f.open("s2t9.dat");
    for (size_t l = 2; l<16; l++) {
        double tmp_mc = 0 , tmp_t = 0 , tmp_cc = 0 , tmp_gl = 0;
        size_t N = pow(2,l) - 1;
        std::vector<double> nodes, weights;

        monte_carlo_rule(l, nodes);
        for (size_t i = 0; i < N ; i++) {
            tmp_mc += f_gamma(gamma, nodes[i]);
        }
        tmp_mc /= N;
        tmp_mc = std::abs((exact_value - tmp_mc)/exact_value);

        trapezoidal_rule(l, nodes, weights);
        for (size_t i = 0; i < N ; i++) {
            tmp_t += weights[i] * f_gamma(gamma, nodes[i]);
        }
        tmp_t = std::abs((exact_value - tmp_t)/exact_value);

        clenshaw_curtis_rule(l, nodes, weights);
        for (size_t i = 0; i < N ; i++) {
            tmp_cc += weights[i] * f_gamma(gamma, nodes[i]);
        }
        tmp_cc = std::abs((exact_value - tmp_cc)/exact_value);

        gauss_legendre_rule(l, nodes, weights,0,1);
        for (size_t i = 0; i < N ; i++) {
            tmp_gl += weights[i] * f_gamma(gamma, nodes[i]);
        }
        tmp_gl = std::abs((exact_value - tmp_gl)/exact_value);

        f << N << ' ' << tmp_mc << ' ' << tmp_t << ' ' << tmp_cc << ' ' << tmp_gl << '\n';
    }
    f.close();
}

// enable doxygen processing for this header:
/** @file */
