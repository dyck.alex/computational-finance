/*!
 *  \date      29. May 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */


#include "../tasks.h"
#include <iostream>
#include <gsl/gsl_rng.h>
#include <vector>
#include <cmath>

/**
* Returns the nodes and weights for the Clenshaw-curtis rule
* @param l level
* @param nodes quadrature nodes
* @param weights quadrature weights
*/
void clenshaw_curtis_rule(int l,std::vector<double> & nodes,std::vector<double> & weights){
    int N=std::pow(2,l)-1;	
    nodes.resize(N);
    weights.resize(N);
    for(int i=0; i<N; i++){
	    nodes[i]=0.5*(1-std::cos(M_PI*(i+1)/(N+1)));
        double factor=(2.0/(N+1))*std::sin(M_PI*(i+1)/(N+1));
        weights[i]=0;
        for(int j=1; j<=(N+1)/2; j++){
             weights[i]+=(1.0/(2*j-1))*std::sin((2*j-1)*M_PI*(i+1)/(N+1));
        }
        weights[i]*=factor;
    }    
}

/**
* Main function
*/
void sheet2_task8() {
    std::vector<double> nodes, weights;
    clenshaw_curtis_rule(3,nodes,weights);

    std::cout << "Clenshaw Curtis" << std::endl;
    std::cout << "Nodes" << std::endl;

    for(size_t i = 0; i<nodes.size(); i++){
	    std::cout << nodes[i] << " ";
    }

    std::cout << std::endl;
    std::cout << "Weights" << std::endl;

    for(size_t i = 0; i<weights.size(); i++){
	    std::cout << weights[i] << " ";
    }
    std::cout << std::endl;
}


// enable doxygen processing for this header:
/** @file */
