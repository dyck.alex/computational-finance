/*!
 *  \date      29. May 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */


#include "../tasks.h"
#include <iostream>
#include <gsl/gsl_rng.h>
#include <cmath>

/**
* Main function
 * Same as in task 1
*/
void sheet2_task2() {
    gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(r,time(NULL));
    int N = 1000;
    std::vector<double> time;
    std::vector<std::vector<double> > BM;
    std::vector<double> Vcall(N);

    // setting parameters
    double S0 = 10 , mu = 0.1 , sigma = 0.2, T= 2, timestep = 0.2, K = 10;

    BM.resize(N,std::vector<double>(1));
    create_equidistant_timesteps(time, T, timestep);
    for(int i = 0; i < N; i++){
        WienerProcess(r, time, BM[i]);
        geometric_brownian_motion(time, BM[i], S0, mu, sigma);
        Vcall[i] = payoff_call(BM[i],K, time.size()-1);
    }

    double mean = empiricalMean(Vcall);
    double sig = empiricalDeviation(Vcall);
    std::cout << "Estimated mean: " << mean << std::endl;
    std::cout << "Estimated sigma " << sig << std::endl;
    
    double timesteps[3] = {0.4,1.0,2.0};
    std::ofstream f;
    f.open("s2t2.dat");
    for(int j=0; j< 3; j++){
        create_equidistant_timesteps(time, T, timesteps[j]);
        for(int i=0; i< N; i++){
            WienerProcess(r, time, BM[i]);
            geometric_brownian_motion(time, BM[i], S0, mu, sigma);
            Vcall[i] = payoff_call(BM[i],K, time.size()-1);
            }
        mean = empiricalMean(Vcall);
        sig = empiricalDeviation(Vcall);
        f << timesteps[j] << ' ' << mean << ' ' << sig <<'\n';
    }
    f.close();
    gsl_rng_free(r);
}
// enable doxygen processing for this header:
/** @file */
