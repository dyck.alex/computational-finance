/*!
 *  \date      29. May 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */


#include "../tasks.h"
#include <iostream>
#include <gsl/gsl_rng.h>
#include <vector>
#include <cmath>

/**
* Returns the nodes and weights for the trapezoidal rule
* @param l level
* @param nodes quadrature nodes
* @param weights quadrature weights
*/
void trapezoidal_rule(int l,std::vector<double> & nodes,std::vector<double> & weights) {
    size_t N = std::pow(2.,l)-1;
    nodes.resize(N);
    weights.resize(N);
    if (l == 1) {
        nodes[0] = 0.5;
        weights[0] = 1;
    }
    else {
        for(size_t i = 0; i < N; i++) {
            nodes[i]= (double)(i+1)/(N+1);
            if( i==0 || i==N-1)
                weights[i] = 3./(2*(N+1));
            else weights[i] = 1./(N+1);
        }
    }
}


/**
* Main function
*/
void sheet2_task6() {
    std::vector<double> nodes, weights;
    trapezoidal_rule(3,nodes,weights);
    std::cout << "Trapezoidal" << std::endl;
    std::cout << "Nodes" << std::endl;
    for(unsigned int i=0; i < nodes.size(); i++){
	    std::cout << nodes[i] << " ";
    }
    std::cout << std::endl;
    std::cout << "Weights" << std::endl;
    for(unsigned int i=0; i < weights.size(); i++){
	    std::cout << weights[i] << " ";
    }
    std::cout << std::endl;
}

// enable doxygen processing for this header:
/** @file */
