/*!
 *  \date      22. May 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */

#include "../tasks.h"
#include <iostream>
#include <gsl/gsl_rng.h>
#include <vector>
#include <cmath>

/**
* the integrand of a European call-option
* @param S0 Initial Value
* @param mu drift
* @param sigma volatility
* @param T expiration date
* @param K strike price
* @param s
*/
double fcall(double S0, double mu, double sigma, double T, double K, double s){
    return std::max(S0 * std::exp((mu-0.5 * sigma * sigma) * T + sigma * std::sqrt(T) * s) - K,0.0);
}

/**
* Main function
* Application of the different integration rules
*/
void sheet2_task10() {
    // setting parameters
    double S0 = 10 , mu = 0.1 , sigma = 0.2, T= 1, K = 0;
    double exact_value = expectation_Vcall_exact(S0,mu,sigma,T,K);

    std::ofstream f;
    f.open("s2t10_K0.dat");

    for (size_t l = 2; l<16; l++) {
        double tmp_mc = 0 , tmp_t = 0 , tmp_cc = 0 , tmp_gl = 0;
        size_t N = pow(2,l) - 1;
        std::vector<double> nodes, weights;

        monte_carlo_rule(l, nodes);
        for (size_t i = 0; i <N ; i++) {
            tmp_mc += fcall(S0,mu,sigma,T,K,NormalInverseCDF(nodes[i]));
        }
        tmp_mc /= N;
        tmp_mc = std::abs((exact_value - tmp_mc)/exact_value);

        trapezoidal_rule(l, nodes, weights);
        for (size_t i = 0; i <N ; i++) {
            tmp_t += weights[i]*fcall(S0,mu,sigma,T,K,NormalInverseCDF(nodes[i]));
        }
        tmp_t = std::abs((exact_value - tmp_t)/exact_value);

        clenshaw_curtis_rule(l, nodes, weights);
        for (size_t i = 0; i <N ; i++) {
            tmp_cc += weights[i]*fcall(S0,mu,sigma,T,K,NormalInverseCDF(nodes[i]));
        }

        tmp_cc = std::abs((exact_value - tmp_cc)/exact_value);

        gauss_legendre_rule(l, nodes, weights,0,1);
        for (size_t i = 0; i <N ; i++) {
            tmp_gl += weights[i]*fcall(S0,mu,sigma,T,K,NormalInverseCDF(nodes[i]));
        }
        tmp_gl = std::abs((exact_value - tmp_gl)/exact_value);

        f << N << ' ' << tmp_mc << ' ' << tmp_t << ' ' << tmp_cc << ' ' << tmp_gl << '\n';
    }

    f.close();
}

// enable doxygen processing for this header:
/** @file */
