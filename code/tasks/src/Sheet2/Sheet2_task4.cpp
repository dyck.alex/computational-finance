/*!
 *  \date      29. May 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */


#include "../tasks.h"
#include <iostream>
#include <gsl/gsl_rng.h>
#include <cmath>

/**
* Return the exact expectation of Vcall
* @param S0 
* @param mu drift
* @param sigma volatility 
* @param T expiration date
* @param K strike price
*/
double expectation_Vcall_exact(double S0, double mu, double sigma, double T, double K){
    double xi = (1.0/(sigma*std::sqrt(T)))*(std::log(K/S0)-(mu-sigma*sigma/2.0)*T);
    return S0*std::exp(mu*T)*NormalCDF(sigma*std::sqrt(T)-xi)-K*NormalCDF(-xi);
}


/**
* Main function
 * executing task4
*/
void sheet2_task4() {
    gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(r,time(NULL));
    std::vector<double> time;
    std::vector<std::vector<double> > BM1,BM2,BM3,BM4,BM5;
    std::vector<double> Vcall1,Vcall2,Vcall3,Vcall4,Vcall5;

    // setting parameters
    double S0 = 10 , mu = 0.1 , sigma = 0.2, T= 1, timestep = 1, K = 10;
    double exact_value = expectation_Vcall_exact(S0,mu,sigma,T,K);
    std::cout << exact_value << '\n';

    create_equidistant_timesteps(time, T, timestep);

    std::ofstream f;
    f.open("s2t4.dat");
    for (auto N = 2; N < 1000000; N *= 2) {
		Vcall1.resize(N);
		Vcall2.resize(N);
		Vcall3.resize(N);
		Vcall4.resize(N);
		Vcall5.resize(N);
    	        BM1.resize(N,std::vector<double>(1));
		BM2.resize(N,std::vector<double>(1));
		BM3.resize(N,std::vector<double>(1));
		BM4.resize(N,std::vector<double>(1));
		BM5.resize(N,std::vector<double>(1));

		for(int i=0; i< N; i++){
			WienerProcess(r, time, BM1[i]);
			WienerProcess(r, time, BM2[i]);
			WienerProcess(r, time, BM3[i]);
			WienerProcess(r, time, BM4[i]);
			WienerProcess(r, time, BM5[i]);
			geometric_brownian_motion(time, BM1[i], S0, mu, sigma);
			geometric_brownian_motion(time, BM2[i], S0, mu, sigma);
			geometric_brownian_motion(time, BM3[i], S0, mu, sigma);
			geometric_brownian_motion(time, BM4[i], S0, mu, sigma);
			geometric_brownian_motion(time, BM5[i], S0, mu, sigma);
			Vcall1[i] = payoff_call(BM1[i],K, time.size()-1);
			Vcall2[i] = payoff_call(BM2[i],K, time.size()-1);
			Vcall3[i] = payoff_call(BM3[i],K, time.size()-1);
			Vcall4[i] = payoff_call(BM4[i],K, time.size()-1);
			Vcall5[i] = payoff_call(BM5[i],K, time.size()-1);
    	}

        double err_mean1 = std::abs(empiricalMean(Vcall1) - exact_value);
        double err_mean2 = std::abs(empiricalMean(Vcall2) - exact_value);
        double err_mean3 = std::abs(empiricalMean(Vcall3) - exact_value);
        double err_mean4 = std::abs(empiricalMean(Vcall4) - exact_value);
        double err_mean5 = std::abs(empiricalMean(Vcall5) - exact_value);
        f << N << ' ' << err_mean1<< ' ' << err_mean2 << ' ' << err_mean3<< ' ' << err_mean4 << ' ' << err_mean5 <<'\n';
	BM1.clear(), BM2.clear() , BM3.clear(), BM4.clear(), BM5.clear();
	Vcall1.clear(), Vcall2.clear() , Vcall3.clear(), Vcall4.clear(), Vcall5.clear();
    }	
    f.close();
    gsl_rng_free(r);
}

// enable doxygen processing for this header:
/** @file */
