/*!
 *  \date      29. May 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */


#include "../tasks.h"
#include <iostream>
#include <gsl/gsl_rng.h>
#include <cmath>

/**
* Payoff function Vcall
* @param process geometric Brownian motion
* @param K strike price
* @param T expiration date
*/
double payoff_call(std::vector <double> & process,double K,double T) {
    return std::max(process[T]-K,0.0);
}

/**
* Main function
 * Initializes random number generator.
 * Time vector and brownian motion is declared as well as a vector to save the
 * values of the call option.
 *
 * Since timestep is equal during the run the times only has to be computed once.
 * Via for - loop we construct a GMB and compute the Call option
*/
void sheet2_task1() {
    gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(r,time(NULL));

    size_t N = 1000;
    std::vector<double> time;
    std::vector<std::vector<double>> BM;
    std::vector<double> Vcall(N);

    // setting parameters
    double S0 = 10 , mu = 0.1 , sigma = 0.2, T= 2, timestep = 0.2, K=10;

    BM.resize(N,std::vector<double>(1));
    create_equidistant_timesteps(time, T, timestep);

    for(size_t i=0; i< N; i++){
        WienerProcess(r, time, BM[i]);
        geometric_brownian_motion(time, BM[i], S0, mu, sigma);
        Vcall[i]=payoff_call(BM[i],K, time.size()-1);
    }

    double mean = empiricalMean(Vcall);
    std::cout << "Estimated mean: " << mean << std::endl;

    //part 2
    double sigmas[5] = {0.0,0.2,0.4,0.6,0.8};

    std::ofstream f;
    f.open("s2t1.dat");
    
    for(size_t j=0; j< 5; j++){
        for(size_t i=0; i< N; i++){
            WienerProcess(r, time, BM[i]);
            geometric_brownian_motion(time, BM[i], S0, mu, sigmas[j]);
            Vcall[i]=payoff_call(BM[i],K, time.size()-1);
        }
        mean = empiricalMean(Vcall);
        f << sigmas[j] << ' ' << mean << '\n';
    }

    f.close();
    gsl_rng_free(r);
}

// enable doxygen processing for this header:
/** @file */
