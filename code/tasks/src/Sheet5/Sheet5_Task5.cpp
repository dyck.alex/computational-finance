/*!
 *  \date      10. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>
#include <algorithm>

//fixed
double discrete_lookback_call_fixed(const std::vector <double> & process, double K){
    double max = *std::max_element(process.begin(),process.end());
    return std::max(max - K,0.0);
}

//variable
double discrete_lookback_call_variable(const std::vector <double> & process){
    double min = *std::min_element(process.begin(),process.end());
    return std::max(process.back() - min,0.0);
}

void sheet5_task5() {
    double S0 = 10. , r = 0.02 , sigma = 0.2 , K = 10, T = 1;
    double t1 = 0.5;
    double t2 = 1.0;
    (void) T;
    //(void) K;
    double value = 0;
    std::ofstream f;
    f.open("s5t5_fixed.dat");
    for(double i = 1.0/50.0; i < 1.0; i+=1.0/50.0){
        for(double j = 1.0/50.0; j < 1.0; j+=1.0/50.0){
            std::vector<double> process(2);
            process[0] = S0*std::exp((r-0.5*sigma*sigma)*t1+sigma*std::sqrt(t1)*NormalInverseCDF(i));
            process[1] = S0*std::exp((r-0.5*sigma*sigma)*t2+sigma*std::sqrt(t2)*NormalInverseCDF(j));
            value = discrete_lookback_call_fixed(process,K);
            f << i << ' ' << j << ' ' << value << '\n';
        }
    }
    f.close();
}
