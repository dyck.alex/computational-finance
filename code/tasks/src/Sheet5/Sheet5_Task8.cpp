/*!
 *  \date      10. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>

double European_Call(double S0, double K , double r, double sigma, double M,double dt) {
    std::vector< std::vector<double> > S(M+1),V(M+1);
    for (auto it = S.begin(); it != S.end() ; it++)
        it->resize(M+1);
    for (auto it = V.begin(); it != V.end() ; it++)
        it->resize(M+1);
    return BinomialMethod_EuropeanCall(V,S,S0,K,sigma,r,M,dt);
}

double Newton_Raphson_algorithm(double V, double S0, double r , double sigma0, double K , double T) {
    for (size_t i = 0; i< 100; i++) {
        double d1 = ((std::log(S0/K) + (r+ 0.5*sigma0*sigma0)*T)/(sigma0 * std::sqrt(T)));
        double v = S0*std::sqrt(T) * standardNormalDensity(d1);
        sigma0 -= (BlackScholesFormula(S0,K,sigma0,r,T) - V)/v;
        //std::cout << d1 << ' ' << v << ' ' <<  sigma0 <<  std::endl;
    }
    return sigma0;
}

void sheet5_task8() {
    gsl_rng* ran = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(ran,time(NULL));
    double S0 = 10. , r = 0.02 , sigma = 0.2 , K = 10, T = 1, M = 32;
    std::vector<double> BM(M), time;
    time_generate(time,T,M);
    std::ofstream f ;

    f.open("s5t8.dat");
    for (K = 0; K < 100; K++) {
        double V = European_Call(S0, K, r, sigma, M, T/M);
        //double sig0 = 2.*V /(std::sqrt(T)* S0), sol = 0;
        double sig0 = 1.3, sol = 0;

        sol = Newton_Raphson_algorithm(V, S0, r, sig0, K, T);
        f << K << ' ' << sol ;
        f << std::endl;
    }
    f.close();
    gsl_rng_free(ran);
}