/*!
 *  \date      10. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>


void sheet5_task4(){
	bool bb = true;
    double S0 = 10. , r = 0.02 , sigma = 0.2 , K = 10, B = 8.5, T = 1;
    std::vector<double> M = {4, 64, 256, 1024};
    std::vector< std::vector<double> > nodes;
    std::vector<double> weights;
    std::vector <double> process, BR;
	std::vector<double> time, time2, time3, time4, time_help;
    //Brownian bridge
	if(bb) { 
		time_generate(time,T,M[0]); 
		time_generate(time2,T,M[1]); 
		time_generate(time3,T,M[2]); 
		time_generate(time4,T,M[3]); 

	} 
    //random walk
	else{
		create_equidistant_timesteps(time, T, T/M[0]);
		create_equidistant_timesteps(time2, T, T/M[1]);
		create_equidistant_timesteps(time3, T, T/M[2]);
		create_equidistant_timesteps(time4, T, T/M[3]);
	}

    double exact_value = down_out_call(S0,r, sigma, K, B, T);
    size_t l;
    size_t N;

    std::cout << exact_value << '\n';

    std::ofstream f;
    f.open("s5t4.dat");

    for (l = 2; l<20; l++) {
        std::cout << " level " << l << '\n';
        double tmp_mc = 0, tmp_mc2 = 0, tmp_mc3 = 0, tmp_mc4 = 0; 
        N = pow(2,l) - 1;

        //M = 4
        monte_carlo_rule_multidimensional(l, M[0], nodes);

        for (size_t i = 0; i < N ; i++) {
			time_help = time;
			if(bb){ 
				WienerProcess_BR(nodes[i], time, BR, T, M[0]);
				nodes[i] = BR;
			}
	    	else {random_walk(time, nodes[i]);}
			process.resize(M[0]);
            create_GBM(process, S0, r, sigma, nodes[i], time);
            tmp_mc += discrete_downout_call(process, K, B);
			time = time_help;
        }
        tmp_mc /= N*std::exp(r*T);
		std::cout << tmp_mc << '\n';
        tmp_mc = std::abs((exact_value - tmp_mc)/exact_value);
		nodes.clear();

        //M = 64
        monte_carlo_rule_multidimensional(l, M[1], nodes);

        for (size_t i = 0; i < N ; i++) {
			time_help = time2;
			if(bb){ 
				WienerProcess_BR(nodes[i], time2, BR, T, M[1]);
				nodes[i] = BR;
			}
	    	else {random_walk(time2, nodes[i]);}
			process.resize(M[1]);
            create_GBM(process, S0, r, sigma, nodes[i], time2);
            tmp_mc2 += discrete_downout_call(process, K, B);
			time = time_help;
        }
        tmp_mc2 /= N*std::exp(r*T);
        tmp_mc2 = std::abs((exact_value - tmp_mc2)/exact_value);
		nodes.clear();

        //M = 256
        monte_carlo_rule_multidimensional(l, M[2], nodes);

        for (size_t i = 0; i < N ; i++) {
			time_help = time;
			if(bb){ 
				WienerProcess_BR(nodes[i], time3, BR, T, M[2]);
				nodes[i] = BR;
			}
	    	else {random_walk(time3, nodes[i]);}
			process.resize(M[2]);
            create_GBM(process, S0, r, sigma, nodes[i], time3);
            tmp_mc3 += discrete_downout_call(process, K, B);
			time = time_help;
        }
        tmp_mc3 /= N*std::exp(r*T);
        tmp_mc3 = std::abs((exact_value - tmp_mc3)/exact_value);
		nodes.clear();

        //M = 1024
        monte_carlo_rule_multidimensional(l, M[3], nodes);

        for (size_t i = 0; i < N ; i++) {
			time_help = time;
			if(bb){ 
				WienerProcess_BR(nodes[i], time4, BR, T, M[3]);
				nodes[i] = BR;
			}
	    	else {random_walk(time4, nodes[i]);}
			process.resize(M[3]);
            create_GBM(process, S0, r, sigma, nodes[i], time4);
            tmp_mc4 += discrete_downout_call(process, K, B);
			time = time_help;
        }
        tmp_mc4 /= N*std::exp(r*T);
        tmp_mc4 = std::abs((exact_value - tmp_mc4)/exact_value);
        nodes.clear();  

        f << N << ' ' << tmp_mc << ' ' << tmp_mc2 << ' ' << tmp_mc3 << ' ' << tmp_mc4 <<  '\n';
    }
    f.close();
}
