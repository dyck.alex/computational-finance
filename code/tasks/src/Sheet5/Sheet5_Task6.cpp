/*!
 *  \date      10. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>
/**
* Black-Scholes formula for the Lookback option with fixed strike
*/
double lookback_call(double S0, double r, double sigma, double T, double K){
	double V = expectation_Vcall_exact(S0, r, sigma, T, S0)*std::exp(-r*T);
	double d = d_SK(S0, S0, r, sigma, T);
	return std::exp(-r*T) * (S0 - K) + V + (S0*sigma*sigma)/(2.*r)*(NormalCDF(d + sigma*sqrt(T)) - std::exp(-r*T)*NormalCDF(-d));
}

/**
* Black-Scholes formula for the Lookback option with variable strike
*/
double lookback_call(double S0, double r, double sigma, double T){
	double V = expectation_Vcall_exact(S0, r, sigma, T, S0)*std::exp(-r*T);
	double d = d_SK(S0, S0, r, sigma, T);
	return V + (S0*sigma*sigma)/(2.*r)*(std::exp(-r*T)*NormalCDF(d) - NormalCDF(-d - sigma*sqrt(T)));	
}

double integrand_discrete_lookback_call(const std::vector <double> & time, const std::vector <double> & process, double S0, double r , double sig, double K ) {
    std::vector <double> pro (process.size());
    for (size_t k = 0 ; k< process.size(); k++) {
        pro[k] = std::sqrt(time[k]) * NormalInverseCDF(process[k]);
    }
    geometric_brownian_motion(time, pro, S0, r, sig);
    return discrete_lookback_call_fixed(pro,K);
}

//Main function for task 2
void sheet5_task6(){
    double S0 = 10. , r = 0.02 , sigma = 0.2 , K = 10, T = 1;
    size_t M = 64;
    std::vector< std::vector<double> > nodes;
    std::vector <double> process, BR;
    std::vector<double> time, time_help;

    //Brownian bridge
    time_generate(time,T,M);
	//create_equidistant_timesteps(time, T, T/M);
    process.resize(M);

    double exact_value = 0;
	std::cout << lookback_call(S0, r, sigma, T, K) << '\n'; 
    size_t lev = 20;
    size_t N = pow(2,lev) - 1;
    halton(nodes, M, N);
	//monte_carlo_rule_multidimensional(lev, M, nodes);

    for (size_t i = 0; i < N ; i++) {
		time_help = time;
		WienerProcess_BR(nodes[i], time, BR, T, M);
		nodes[i] = BR;
		//random_walk(time, nodes[i]);
        create_GBM(process, S0, r, sigma, nodes[i], time);
        exact_value += discrete_lookback_call_fixed(process, K);
		time = time_help;
    }
    exact_value /= N*std::exp(r*T);
    std::cout << "Exact value: " << exact_value << '\n'; return;

    std::ofstream f;
    f.open("s5t6.dat");

    for (size_t l = 2; l<20; l++) {
        std::cout << " level " << l << '\n';
        double tmp_mc = 0, tmp_qmc = 0;
        N = pow(2,l) - 1;

        //Monte Carlo
        monte_carlo_rule_multidimensional(l, M, nodes);

        for (size_t i = 0; i < N ; i++) {
            create_GBM(process, S0, r, sigma, nodes[i], time);
            tmp_mc += discrete_lookback_call_fixed(process, K);
        }
        tmp_mc /= N;
        tmp_mc = std::abs((exact_value - tmp_mc)/exact_value);
        nodes.clear();

        //Quasi Monte Carlo
        halton(nodes, M, N);
        for (size_t i = 0; i < N ; i++) {
            create_GBM(process, S0, r, sigma, nodes[i], time);
            tmp_qmc += discrete_lookback_call_fixed(process, K);
        }
        tmp_qmc /= N;
        tmp_qmc = std::abs((exact_value - tmp_qmc)/exact_value);
        nodes.clear();


        f << N << ' ' << tmp_mc << ' ' << tmp_qmc << '\n';
    }
    f.close();
}
