/*!
 *  \date      10. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>

/**
* payoff function of a discrete barrier Down-Out Call option
* @param process geometric Brownian motion
* @param K strike price
* @param B barrier
* @param T expiration date
*/
double discrete_downout_call(std::vector <double> & process, double K, double B){
    for(auto it=process.begin(); it != process.end(); ++it){
        if((*it)<=B) return 0;
    }
    return std::max(process.back()-K,0.0);
}

//Main function for task 1
void sheet5_task1(){
    double S0 = 10. , r = 0.02 , sigma = 0.2 , K = 10, B = 2;
    double t1 = 0.5;
    double t2 = 1.0;
    double value = 0.;
    std::ofstream f;
    f.open("s5t1.dat");
    for(double i = 1.0/50.0; i < 1.0; i+=1.0/50.0){
        for(double j = 1.0/50.0; j < 1.0; j+=1.0/50.0){
            std::vector<double> process(2);
            process[0] = S0*std::exp((r-0.5*sigma*sigma)*t1+sigma*std::sqrt(t1)*NormalInverseCDF(i));
            process[1] = S0*std::exp((r-0.5*sigma*sigma)*t2+sigma*std::sqrt(t2)*NormalInverseCDF(j));
            value = discrete_downout_call(process, K, B);
            f << i << ' ' << j << ' ' << value << '\n';
        }
    }
    f.close();
    
}



