/*!
 *  \date      10. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>

double integrand_discrete_asian_average(const std::vector <double> & time, const std::vector <double> & process, double S0, double r , double sig, double K ) {
    std::vector <double> pro (process.size());
    for (size_t k = 0 ; k< process.size(); k++) {
        pro[k] = std::sqrt(time[k]) * NormalInverseCDF(process[k]);
    }
    geometric_brownian_motion(time, pro, S0, r, sig);
    return Asian::discrete_arithmetic_average(pro,K);
}

//Main function for task 2
void sheet5_task7(){
    double S0 = 10. , r = 0.02 , sigma = 0.2 , K = 10, T = 1;
    size_t M = 64;
    std::vector< std::vector<double> > nodes;
    std::vector <double> process;
    std::vector<double> time;

    //Brownian bridge
    //time_generate(time,T,M);
	create_equidistant_timesteps(time, T, T/M);
    process.resize(M);

    double exact_value = 0;
    size_t lev = 20;
    size_t N = std::pow(2.,lev) - 1;
    halton(nodes, M, N);
	//monte_carlo_rule_multidimensional(lev, M, nodes);

    for (size_t i = 0; i < N ; i++) {
		random_walk(time, nodes[i]);
        create_GBM(process,S0,r,sigma,nodes[i],time);
        exact_value += Asian::discrete_arithmetic_average(process, K);
    }

    exact_value /= N*std::exp(r*T);

    std::cout << "Exact value: " << exact_value << '\n';
    double exact_value2 = Asian::Vcall_discrete(S0,r,sigma,T,K,
                                                static_cast<double>(T)/M,M);
    //std::cout << "Exact value2: " << exact_value2 << '\n';  
    std::ofstream f;
    f.open("s5t7.dat");

    for (size_t l = 2; l<20; l++) {
        std::cout << " level " << l << '\n';
        double tmp_mc = 0, tmp_qmc = 0, tmp_mc2 = 0, tmp_qmc2 = 0;
        N = pow(2,l) - 1;

        //Monte Carlo
        monte_carlo_rule_multidimensional(l, M, nodes);

        for (size_t i = 0; i < N ; i++) {
			random_walk(time, nodes[i]);
            create_GBM(process, S0, r, sigma, nodes[i], time);
            tmp_mc += Asian::discrete_arithmetic_average(process, K);
			tmp_mc2+= (Asian::discrete_arithmetic_average(process, K) - Asian::discrete_geometric_average(process, K));
        }
        tmp_mc /= N*std::exp(r*T);
		tmp_mc2 /= N*std::exp(r*T);
		tmp_mc2+=exact_value2; 
		std::cout << tmp_mc << ' ' << tmp_mc2 << '\n';
        tmp_mc2 = std::abs((exact_value - tmp_mc2)/exact_value);
        tmp_mc = std::abs((exact_value - tmp_mc)/exact_value);
        nodes.clear();

        //Quasi Monte Carlo
        halton(nodes, M, N);
        for (size_t i = 0; i < N ; i++) {
			random_walk(time, nodes[i]);
            create_GBM(process, S0, r, sigma, nodes[i], time);
            tmp_qmc += Asian::discrete_arithmetic_average(process, K);
			tmp_qmc2+= (Asian::discrete_arithmetic_average(process, K) - Asian::discrete_geometric_average(process, K));
        }
        tmp_qmc /= N*std::exp(r*T);
		tmp_qmc2 /= N*std::exp(r*T);
		tmp_qmc2+=exact_value2;
		std::cout << tmp_qmc << ' ' << tmp_qmc2 << '\n';
        tmp_qmc2 = std::abs((exact_value - tmp_qmc2)/exact_value);
        tmp_qmc = std::abs((exact_value - tmp_qmc)/exact_value);
        nodes.clear();

        f << N << ' ' << tmp_mc << ' ' << tmp_qmc << ' ' <<  tmp_mc2 << ' ' << tmp_qmc2 <<  '\n';
    }
    f.close();
}
