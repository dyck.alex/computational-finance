/*!
 *  \date      10. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>

void sheet5_task9() {
    gsl_rng* ran = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(ran,time(NULL));
    double S0 = 24.875  , r = 0.02 ,T = 1, M = 32;
    std::vector<double> BM(M), time;
    time_generate(time,T,M);
    std::ofstream f ;

    //double K[10] = {340, 350, 360, 370, 380, 390, 400, 410 ,420, 430};
    //Maturity is in one day, so I leave T = 1 with 24 ours
    //double V = 1e-3;

    double K[10] = {22,22.5,23,23.5,24,24.5,25,25.5,26,26.5};
    double V[10] = {4.98,4.66,4.34, 4.04, 3.75, 3.47,3.2,2.94,2.7,2.47};
    f.open("s5t9.dat");
    for (size_t i = 0; i< 10 ; i++) {
        //double sig0 = 2.*V[i] /(std::sqrt(T)* S0), sol = 0;
        double sig0 = 0.1  , sol = 0;

        sol = Newton_Raphson_algorithm(V[i], S0, r, sig0, K[i], T);
        f << K[i] << ' ' << sol ;
        f << std::endl;
    }
    f.close();
    gsl_rng_free(ran);
}