/*!
 *  \date      10. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>

/**
* d from the Black-Scholes formula for a European Call option
*/
double d_SK(double S, double K, double r, double sigma, double T){
	return (1.0/(sigma*std::sqrt(T)))*(std::log(S/K)+(r-sigma*sigma/2.0)*T);
}

/**
* Black-Scholes formula for the Down-Out Call option
*/
double down_out_call(double S0, double r, double sigma, double K, double B, double T){
	double Z = std::pow(B/S0,(2.0*r)/(sigma*sigma)-1.0);
	double B_bar = std::max(B,K);
	double V1 = expectation_Vcall_exact(S0, r, sigma, T, B_bar)*std::exp(-r*T);
	double V2 = expectation_Vcall_exact((B*B)/S0, r, sigma, T, B_bar)*std::exp(-r*T);
	double d1 = d_SK(S0, B_bar, r, sigma, T);
	double d2 = d_SK((B*B)/S0, B_bar, r, sigma, T);
	return V1 - Z*V2 + (B_bar - K)*std::exp(-r*T)* (NormalCDF(d1) - Z*NormalCDF(d2) );
}

void sheet5_task3(){
	double S0 = 10. , r = 0.02 , sigma = 0.2 , K = 10, T = 1;
	//std::cout << down_out_call(S0, r, sigma, K, 8.5, T) << '\n'; return;
    std::ofstream f;
    f.open("s5t3.dat");
	for(double B = 0.0; B < 10; B+=0.1){
		std::cout << B << '\n';
		double value = down_out_call(S0, r, sigma, K, B, T);
		f << B << ' ' << value << '\n';
	}
	f.close();
}
