/*!
 *  \date      10. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 */

#include "../tasks.h"
#include <cmath>
#include <numeric>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <fstream>
#include <algorithm>

/**
* geometric brownian motion for M timesteps
* @param process geometric Brownian motion
* @param S0 Starting price
* @param r interest rate
* @param sigma variance 
* @param M number of timesteps
* @param T expiration date
* @param nodes
* @param time
*/
void create_GBM(std::vector <double> & process, double S0, double r, double sigma, const std::vector <double> & nodes, const std::vector<double> & time){
    for (size_t i=1 ; i< time.size(); i++) {
        process[i-1] = S0 * exp((r - pow(sigma,2.)/2.)*time[i]
                                   + sigma*nodes[i-1]);
    }
}

/*
Creates the Wiener Process W(t_i) by random walk construction
* @param time equidistant time steps
* @param nodes
*/
void random_walk(const std::vector<double> & time, std::vector <double> & nodes){
	double value;
	double delta = time[1] - time[0];
	value = NormalInverseCDF(nodes[0])*sqrt(delta);
	nodes[0] = value;
	for(size_t i = 1; i < nodes.size(); i++){
		value = nodes[i-1] + std::sqrt(delta)*NormalInverseCDF(nodes[i]);
		nodes[i] = value;
	}
}

//Main function for task 2
void sheet5_task2(){
	bool bb = true; 
    double S0 = 10. , r = 0.02 , sigma = 0.2 , K = 10, B = 8.5, T = 1;
    size_t M = 64;

    std::vector< std::vector<double> > nodes;
    std::vector<double> weights;
    std::vector <double> process, BR;
	std::vector<double> time, time_help;
    //Brownian bridge
	if(bb){
    	time_generate(time,T,M);
	}
	else{
        //random walk
    	create_equidistant_timesteps(time, T, T/M);
	}

    process.resize(M); 

    double exact_value = 0;
    size_t l = 4;
    size_t N = pow(2,l) - 1;

   	monte_carlo_rule_multidimensional(l, M, nodes);
    //halton(nodes,l,N);
    for (size_t i = 0; i < N ; i++) {
			time_help = time;
			if(bb){ 
				WienerProcess_BR(nodes[i], time, BR, T, M);
				nodes[i] = BR;
			}
			else {
                random_walk(time, nodes[i]);
            }
            create_GBM(process, S0, r, sigma, nodes[i], time);
			if(i == 0){
				for(size_t j = 0 ; j < process.size(); j++){
					std::cout << time[j+1] << ' ' <<  process[j] << '\n';
				}
			}
            exact_value += discrete_downout_call(process, K, B);
            std::cout <<"discrete downout: "<< discrete_downout_call(process, K, B)  << "exact: " << exact_value << '\n';
	    	time = time_help;
    }

    exact_value *= std::exp(-r*T);
    exact_value /= N;
    std::cout << "exact: " << exact_value << '\n';
    exact_value = 0.845211;
    std::ofstream f;
    f.open("s5t2.dat");

    for (l = 2; l<15; l++) {
        std::cout << " level " << l << '\n';
        double tmp_mc = 0, tmp_qmc = 0; 
        N = pow(2,l) - 1;

        //Monte Carlo
        monte_carlo_rule_multidimensional(l, M, nodes);

        for (size_t i = 0; i < N ; i++) {
			time_help = time;
			if(bb){ 
				WienerProcess_BR(nodes[i], time, BR, T, M);
				nodes[i] = BR;
			}
			else random_walk(time, nodes[i]);
            create_GBM(process, S0, r, sigma, nodes[i], time);
            tmp_mc += discrete_downout_call(process, K, B);
	        time= time_help;
        }
        tmp_mc /= N*std::exp(-r*T);
        tmp_mc = std::abs((exact_value - tmp_mc)/exact_value);
        nodes.clear();
        
        //Quasi Monte Carlo
        halton(nodes, M, N);
        for (size_t i = 0; i < N ; i++) {
			time_help = time;
			if(bb){ 
				WienerProcess_BR(nodes[i], time, BR, T, M);
				nodes[i] = BR;
			}
			else random_walk(time, nodes[i]);
            create_GBM(process, S0, r, sigma, nodes[i], time);
            tmp_qmc += discrete_downout_call(process, K, B);
	    time = time_help;
        }
        tmp_qmc /= N*std::exp(r*T);
        tmp_qmc = std::abs((exact_value - tmp_qmc)/exact_value);
        nodes.clear();
        weights.clear();   

        f << N << ' ' << tmp_mc << ' ' << tmp_qmc << '\n';
    }
    f.close();
}
