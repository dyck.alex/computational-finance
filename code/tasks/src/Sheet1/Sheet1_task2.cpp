/*!
 *  \date      09. May 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */


#include "../tasks.h"
#include<cstdlib>
#include<cstdio>
#include<gsl/gsl_rng.h>
#include<cmath>
#include<iostream>
#include<fstream>
#include <time.h>

/**
 *
 * Draws a random number bewtween \f$[a,b]\f$ via rescaling of rand.
 *
 * @param a lower interval bound
 * @param b upper interval bound
 *
 * @return The drawn random number.
 */
double randomNumberab(double a, double b)  {
    double rnd = randomNumber01();

  	return rnd*(b-a) +a;
}


/**
 * Draws a random number bewtween \f$[a,b]\f$ via rescaling of \f$[0,1]\f$ variable.
 *
 * @param r A pointer to the random number generator which is used
 * @param a lower interval bound
 * @param b upper interval bound
 *
 * @return The drawn random number.
 */
double randomNumberabGSL(gsl_rng * r, double a, double b)  {
    double rnd = randomNumber01GSL(r);
    return rnd*(b-a) +a;
}

/**
 * Computes the normal density \f$ \frac{1}{\sqrt{2\pi}}\exp\left(-\frac{x^2}{2}\right) \f$
 *
 * @param x Point of evaluation
 *
 * @return \f$p(x)\f$
 */
double standardNormalDensity(double x) {
    return (1./sqrt(2*M_PI))*exp(-x*x /2.);
}

/**
 * The rejection sampling algorithm (Task 2)
 *
 * @param gen a random number generator
 * @param left_bound the left interval bound
 * @param right_bound the right interval bound
 * @return a standard normal distributed value
 */

double rejectionSampling(gsl_rng * r, double left_bound, double right_bound) {
    double maximum = 0.0;

    if(left_bound<=0 and right_bound>=0)
        maximum = 1.0 / sqrt(2.0 * M_PI);
    else {
        if(left_bound>0)
            maximum = standardNormalDensity(left_bound);
        else
            maximum = standardNormalDensity(right_bound);
    }
    double x = 0, y = 0;

    do {
        //uniformly distributed random variable in [left_bound,right_bound]
        x = left_bound + (right_bound - left_bound) * gsl_rng_uniform(r);
        //uniformly distributed random variable in [0,help_variable]
        y = maximum * gsl_rng_uniform(r);

    } while (y > standardNormalDensity(x));

    return x;
}

// enable doxygen processing for this header:
/** @file */

/**
 * Main function for creation of a data with rejection sampling.
 *
 * A vector with size 1000000 is created and filled with rejectionSampling()
 *
 * printToFile is a template function which is writing data into a file.
 */

void sheet1_task2() {
    std::vector<double> rsSamples(1000000);
    gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(r,time(NULL));
    double a = -7, b = 7; /**< The range \f$[-7,7]\f$ gives the wanted value  */
    //maximum of the density of the standard normal distribution in [left_bound, right_bound]

    for (auto it = rsSamples.begin(); it != rsSamples.end(); it++)
        *it = rejectionSampling(r,a,b);
    printToFile<std::vector<double> > (rsSamples,"rejectionSampling.dat");
    gsl_rng_free(r);
}
