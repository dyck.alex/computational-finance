/*!
 *  \date      09. May 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */

#include "../tasks.h"
#include <fstream>
#include <cmath>

/**
 *
 * \var mu Mean
 * \var sigma array of sigma values. Iteration happens over these values
 */

/**
 * Main function to compute the error of the empirical deviation and mean.
 * Saves the estimated deviation for different sample sizes into files.
 *
 *
 */
void sheet1_task9() {
    gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(r,time(NULL));

    std::ofstream f,g;
    double mu = 2;
    double sigma[3] = {0.1,1.,10.};

    f.open("sigma_estimation.dat");
    g.open("mean_estimation.dat");
    f << 1 << ' ' << sigma[0] << ' ' << sigma[1] << ' ' << sigma[2] << "\n";
    g << 1 << ' ' << mu << ' ' << mu << ' ' << mu  <<  '\n';
    for (auto i = 2; i< 10000000; i*=2) {
        f << i << ' ';
        g << i << ' ';
        for (const double sig :sigma) {
            std::vector<double> samples(i);
            for (auto it = samples.begin(); it != samples.end(); it++)
                *it = mu + sig * moro_sampling(r);
            f << empiricalDeviation(samples)  << ' ';
            g << empiricalMean(samples)  << ' ';
        }
        f << '\n';
        g << '\n';
    }

    f.close();
    g.close();
    gsl_rng_free(r);
}
// enable doxygen processing for this header:
/** @file */

