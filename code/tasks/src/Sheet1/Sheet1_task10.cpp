/*!
 *  \date      09. May 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */

#include "../tasks.h"
#include <gsl/gsl_rng.h>
#include <time.h>
#include <cmath>
#include <vector>
#include <iostream>
#include <string>
// enable doxygen processing for this header:
/** @file */

/**
 *
 * Creates a wiener process
 * @param r random number generator
 * @param time time values for the process
 * @param process The wiener process will be saved into this parameter
 */
void WienerProcess(gsl_rng * r, const std::vector <double> & time, std::vector<double> & process) {
    double rnd =0;
    process.resize(time.size());
    process[0] = 0;

    for (unsigned int i = 1; i< process.size(); i++) {
        rnd = moro_sampling(r);
        process[i] = process[i-1] + sqrt(time[i]-time[i-1])*rnd;
    }
}

/**
 * Create equidistant timesteps and save them into a vector
 * @param time_points Time values are saved in this parameter
 * @param T Time points are created in \f$ [0,T] \f$
 * @param timestep The width of a time step \f$ \Delta t \f$
 */
void create_equidistant_timesteps(std::vector <double> & time_points, double T, double timestep) {
    time_points.resize(static_cast<int>(T/timestep) +1);
    time_points[0] = 0;
    for (unsigned int k = 1; k<time_points.size(); k++) {
        time_points[k] = k * timestep;
    }
}

/**
 *
 * @param time time values
 * @param process Wiener process created using time as time values
 * @param S0 Starting price
 * @param mu mean
 * @param sigma variance
 */
void geometric_brownian_motion(const std::vector<double> & time,std::vector <double> & process, double S0, double mu, double sigma) {
    process[0] = S0;
    for (unsigned int i=1 ; i< time.size(); i++) {
        process[i] = S0 * exp((mu - pow(sigma,2.)/2.)*time[i]
                                   + sigma*process[i]
                          );
    }
}

/**
 * Main function to create files filled with the Process for the different time steps.
 * First, one creates the time steps, because they are equal for the three paths.
 * Second, we simulate a Wiener process into the corresponding BM vector
 * Third, Given the wiener process, one overrides the values by the asset price.
 *
 * In between the files are written.
 */
void sheet1_task10() {
    gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(r,time(NULL));
    std::ofstream f;
    std::vector<double> time, BM1, BM2, BM3;

    //**<================= First time step ==========================
    double S0 = 10 , mu = 0.1 , sigma = 0.2, T= 2, timestep = 0.5;

    create_equidistant_timesteps(time, T, timestep);
    WienerProcess(r, time, BM1);
    WienerProcess(r, time, BM2);
    WienerProcess(r, time, BM3);

    f.open("wiener0_5.dat");
    for (unsigned int i = 0; i<time.size() ; i++) {
        f << time[i] << ' ' << BM1[i] << ' ' << BM2[i] << ' ' <<BM3[i] << '\n';
    }
    f.close();

    geometric_brownian_motion(time, BM1, S0, mu, sigma);
    geometric_brownian_motion(time, BM2, S0, mu, sigma);
    geometric_brownian_motion(time, BM3, S0, mu, sigma);

    f.open("stepsize0_5.dat");
    for (unsigned int i = 0; i<time.size() ; i++) {
        f << time[i] << ' ' << BM1[i] << ' ' << BM2[i] << ' ' <<BM3[i] << '\n';
    }
    f.close();

    //**< ================= Second time step ==========================
    S0 = 10 , mu = 0.1 , sigma = 0.2, T= 2, timestep = 0.01;
    time.clear(), BM1.clear(), BM2.clear() , BM3.clear();
    create_equidistant_timesteps(time, T, timestep);

    WienerProcess(r, time, BM1);
    WienerProcess(r, time, BM2);
    WienerProcess(r, time, BM3);

    f.open("wiener0_01.dat");
    for (unsigned int i = 0; i<time.size() ; i++) {
        f << time[i] << ' ' << BM1[i] << ' ' << BM2[i] << ' ' <<BM3[i] << '\n';
    }
    f.close();

    geometric_brownian_motion(time, BM1, S0, mu, sigma);
    geometric_brownian_motion(time, BM2, S0, mu, sigma);
    geometric_brownian_motion(time, BM3, S0, mu, sigma);

    f.open("stepsize0_01.dat");
    for (unsigned int i = 0; i<time.size() ; i++) {
        f << time[i] << ' ' << BM1[i] << ' ' << BM2[i] << ' ' <<BM3[i] << '\n';
    }
    f.close();

    gsl_rng_free(r);
}
