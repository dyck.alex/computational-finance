/*!
 *  \date      09. May 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */


#include "../tasks.h"
#include <vector>
#include <cmath>
#include <iostream>

/**
 *
 * Computes an estimator for the variance of a given list.
 * @param values A vector of doubles
 * @return estimation of \f$ \sigma \f$
 */
double empiricalDeviation(const std::vector<double>& values) {
    unsigned int N = values.size();
    double alpha = values[0];
    double beta = 0;
    for (unsigned int i = 1; i < N; i++) {
        double gamma = values[i] - alpha;
        alpha = alpha + gamma / (i + 1);
        beta = beta + gamma * gamma * i / (i + 1);
    }
    return sqrt(beta / (N-1));
}

/**
 * Computes the empirical mean \f$ \frac{1}{N} \sum_{i=1}^{N}x_i \f$
 * @param values vector filled with doubles
 * @return Estimation of the mean value
 */
double empiricalMean(const std::vector<double>& values) {
    double mu = 0;
    for (auto const& val : values)  {
        //std::cout << val << std::endl;
        mu += val;
    }
    mu /= values.size();
    return mu;
}

/**
 * Computes an estimator for the variance of a given list via the corrected variance estimator
 * \f$ \frac{1}{N-1} \sum_{i=1}^N (x_i-\mu)^2 \f$.
 * @param values A vector of doubles. Used for computation of the empirical mean.
 * @return estimation of \f$ \sigma \f$
 */
double empiricalDeviation_naive(const std::vector<double>& values) {
    double sig = 0, mu = empiricalMean(values);

    for (auto const& val : values)
        sig += pow(val-mu,2.);
    sig /= values.size() - 1;

    return sqrt(sig);
}


// enable doxygen processing for this header:
/** @file */

/**
 * Main function to compare both variance functions.
 * Creating a vector with size 1000, filling it with normal distributed
 * random variables created via inverse CDF
 *
 * prints out both estimators to terminal.
 */
void sheet1_task8() {
    std::vector<double> samples(1000);
    gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(r,time(NULL));

    for (auto it = samples.begin(); it != samples.end(); it++) {
        *it = moro_sampling(r);
    }

    gsl_rng_free(r);
    std::cout << "Naive empirical Variance " << empiricalDeviation_naive(samples) <<'\n'
              << "Algorithm for empirical Variance " << empiricalDeviation(samples) << std::endl;
}
