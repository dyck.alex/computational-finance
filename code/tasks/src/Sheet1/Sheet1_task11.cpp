/*!
 *  \date      09. May 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */


#include "../tasks.h"
#include <iostream>
#include <gsl/gsl_rng.h>
#include <cmath>

/**
 *
 * Main function to test an approach to estimate \f$ \sigma , \mu \f$ from the
 * geometric brownian motion. The approach uses the distribution of
 * \f$ \log(S(t)/S(0)) = \left(\mu - \frac{1}{2} \sigma^2\right)t + \sigma W(t) \f$
 *
 * First, one creates the time steps.
 * Second, we simulate a Wiener process
 * Third, Given the wiener process, one overrides the values by the asset price.
 *
 * After arithmetic operations, one achieves an approximation of \f$ \mu, \sigma \f$
 *
 * In between the files are written.
 */
void sheet1_task11() {
    gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(r,time(NULL));
    std::vector<double> time, BM;

    // setting parameters
    double S0 = 10 , mu = 0.1 , sigma = 0.2, T= 1, timestep = 10e-3;

    create_equidistant_timesteps(time, T, timestep);
    WienerProcess(r, time, BM);
    geometric_brownian_motion(time, BM, S0, mu, sigma);

    // transformation of geometric BM
    std::vector<double> log_BM(BM.size());
    for (unsigned int i = 1; i < BM.size(); i++) {
        log_BM[i-1]= log(BM[i]/S0)-log(BM[i-1]/S0);
    }
    double sig = empiricalDeviation(log_BM)/sqrt(timestep);
    double mean = (empiricalMean(log_BM)/timestep + 1./2 * pow(sig,2.));

    std::cout << "Estimated mean " << mean << std::endl;
    std::cout << "Estimated sigma " << sig << std::endl;
    gsl_rng_free(r);
}

// enable doxygen processing for this header:
/** @file */
