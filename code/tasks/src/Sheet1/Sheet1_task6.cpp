/*!
 *  \date      09. May 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */

#include "../tasks.h"
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <tuple>
#include <fstream>
#include <vector>

//typedefs
typedef std::pair<double,double> twodRV;

/**
 *
 * Computes a two dimensional Variable via Box-Müller method. Both the values are saved in a std::pair
 * which is accessable via .first and .second.
 *
 * @return std::pair with normal distributed random numbers
 */
twodRV boxMueller(gsl_rng * r) {
    double u1 = randomNumber01GSL(r), u2 = randomNumber01GSL(r);
    double z1 = sqrt(-2*log(u1))*cos(2*M_PI*u2),
           z2 = sqrt(-2*log(u1))*sin(2*M_PI*u2);
    twodRV u(z1,z2) ;
    return u;
}
// enable doxygen processing for this header:
/** @file */

/**
 *
 * @typedef twodRV std::pair
 */

/**
 * Main function for creation of a data via Box-Müller.
 * Append 1000 elements to the file.
 * */
void sheet1_task6() {
    gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);

    std::ofstream f;
    f.open("boxmueller.dat");

    for (int i = 0; i<1000; i++) {
        twodRV z = boxMueller(r);
        f << z.first << ' ' << z.second << "\n";
    }
    f.close();
    gsl_rng_free(r);
}
