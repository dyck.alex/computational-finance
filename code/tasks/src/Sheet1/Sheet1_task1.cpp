/*!
 *  \date      09. May 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */

#include "../tasks.h"
#include<cstdlib>
#include<iostream>
#include<gsl/gsl_rng.h>
#include<gsl/gsl_randist.h>

/**
 *
 * Draws a random number bewtween \f$[0,1]\f$ via rand.
 *
 * @return The drawn random number.
 */
double randomNumber01()
{
	return (double) rand() / RAND_MAX; // *
}

/**
 * Draws a random number in \f$[0,1]\f$ via the gsl.
 *
 * @param r A pointer to the random number generator which is used.
 *
 * @return The drawn random number.
 */
double randomNumber01GSL(gsl_rng* r)
{
	return gsl_rng_uniform(r);
}

/**
 * Draws a random number in \f$[0,1]\f$ via the gsl_ran_gaussian.
 *
 * @param r A pointer to the random number generator which is used.
 *
 * @return The drawn standard normal distributed random  number.
 */
double randomNumberGaussian (gsl_rng * r) {
    return gsl_ran_gaussian(r,1.);
}

// enable doxygen processing for this header:
/** @file */


/** 
 * Main function for random number evaluation.
 * A first random number \f$x_1\f$ is drawn via rand. 
 * A second random number \f$x_2\f$ is drawn via gsl_rng_uniform.
 * A thrid random number \f$x_3\f$ is drawn via gsl_ran_gaussian
 */

void sheet1_task1() {
    gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);

    std::cout << "Random number via rand() " <<randomNumber01() << std::endl;
    std::cout << "Random number via GSL " << randomNumber01GSL(r) << std::endl;
    std::cout << "Random normal distributed     number via GSL" << randomNumberGaussian(r) << std::endl;

    gsl_rng_free(r);

}
