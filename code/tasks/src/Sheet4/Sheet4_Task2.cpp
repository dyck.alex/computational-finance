/*!
 *  \date      30. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */

#include "../tasks.h"
#include <iostream>
#include <cmath>
#include <numeric>
#include <vector>
#include <fstream>


/**
 * @param S0 Starting price
 * @param K Strike price
 * @param sigma variance
 * @param r risk factor
 * @param T end time point
 * @return evaluation of the Black Scholes Formula
 */
double BlackScholesFormula(double S0, double K, double sigma, double r, double T) {
    double d1 = (log(S0/K) + (r+ sigma*sigma/2.)*T)/(sigma*std::sqrt(T));
    double d2 = d1 - sigma* std::sqrt(T);

    return S0 * NormalCDF(d1) - K* std::exp((-r)*T)* NormalCDF(d2);
}

/**
* Main function
*/
void sheet4_task2() {
    std::vector<std::vector<double> > V,S;
    // setting parameters
    double S0 = 10, K = 10, sigma = 0.2, T= 2, r = 0.2;
    double BS = BlackScholesFormula(S0, K, sigma, r, T);

    std::ofstream f;
    f.open("s4t2.dat");
    for (size_t i = 1 ; i <= 20; i++) {
        V.resize(i*i+1);
        for (auto it = V.begin(); it != V.end(); it++)
            it->resize(i*i+1);
        S.resize(i*i+1);
        for (auto it = S.begin(); it != S.end(); it++)
            it->resize(i*i+1);

        double VCall = BinomialMethod_EuropeanCall(V, S, S0, K, sigma, r,i*i, T/(i*i));
    	f << i*i << ' ' << std::abs(VCall - BS) <<'\n';
    }
    f.close();
}

// enable doxygen processing for this header:
/** @file */