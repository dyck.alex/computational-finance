/*!
 *  \date      1. July 2017
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */

#include "../tasks.h"
#include <iostream>
#include <cmath>
#include <numeric>
#include <vector>
#include <fstream>

/**
 * Binomial Method for a European Put option
 * @param V Value function
 * @param S Process
 * @param S0 Initial price
 * @param K Strike price
 * @param sigma variance
 * @param r risk factor
 * @param M level
 * @param delta_t timestep
 * @return A fair price for an European Put option
 */
double BinomialMethod_EuropeanPut(std::vector<std::vector<double> > &V, std::vector<std::vector<double> > &S,
                                  double S0, double K, double sigma, double r, double M, double delta_t) {
    double beta = (std::exp((-r)*delta_t) + std::exp((r+sigma*sigma)*delta_t))/2.;
    double u = beta + std::sqrt(beta*beta-1);
    double d = beta - std::sqrt(beta*beta-1);
    double p = (std::exp(r*delta_t)-d) / (u-d);


    S[0][0] = S0;
    for (int i = 1; i < M+1; i++) {
        for (int j = 0; j <= i; j++) {
            S[j][i] = S[0][0] * pow(u, j) * pow(d, i - j);
        }
    }

    for (int j = 0; j < M+1; j++) {
        V[j][M] = std::max( K-S[j][M], 0.);
    }

    for (int i = M-1; i >= 0; i--)
        for (int j = 0; j <= i; j++) {
            V[j][i] = std::exp((-r)*delta_t)* (p*V[j+1][i+1]+ (1-p)*V[j][i+1]);
        }

    return V[0][0];
}

/**
 * Binomial Method for a American put option
 * @param V Value function
 * @param S Process
 * @param S0 Initial price
 * @param K Strike price
 * @param sigma variance
 * @param r risk factor
 * @param M level
 * @param delta_t timestep
 * @return A fair price for an American put option
 */
double BinomialMethod_AmericanPut(std::vector<std::vector<double> > &V, std::vector<std::vector<double> > &S,
                                  double S0, double K, double sigma, double r, double M, double delta_t) {
    double beta = (std::exp((-r)*delta_t) + std::exp((r+sigma*sigma)*delta_t))/2.;
    double u = beta + std::sqrt(beta*beta-1);
    double d = beta - std::sqrt(beta*beta-1);
    double p = (std::exp(r*delta_t)-d) / (u-d);


    S[0][0] = S0;
    for (int i = 1; i < M+1; i++) {
        for (int j = 0; j < i+1; j++) {
            S[j][i] = S[0][0] * pow(u, j) * pow(d, i - j);
        }
    }

    for (int j = 0; j < M+1; j++) {
        V[j][M] = std::max( K-S[j][M], 0.);
    }

    for (int i = M-1; i >= 0; i--)
        for (int j = 0; j < i+1; j++) {
            V[j][i] = std::max(std::max(K- S[j][i],0.), std::exp((-r)*delta_t)* (p*V[j+1][i+1]+ (1-p)*V[j][i+1]));
        }

    return V[0][0];
}


/**
* Main function
*/


void sheet4_task3() {
    std::vector<std::vector<double> > V,S;

    // setting parameters
    double S0 = 0, K = 10, sigma = 0.2, T= 2, r = 0.2;

    double M = 64;

    V.resize(M+1);
    for (auto it = V.begin(); it != V.end(); it++)
        it->resize(M+1);
    S.resize(M+1);
    for (auto it = S.begin(); it != S.end(); it++)
        it->resize(M+1);

    std::ofstream f;
    f.open("s4t3.dat");
    for (double i = 0; i <= 10; i+=0.02) {
        S0 = 2*i;
        double VEuropeanPut = BinomialMethod_EuropeanPut(V, S, S0, K, sigma, r, M, T/M),
               VAmericanPut = BinomialMethod_AmericanPut(V, S, S0, K, sigma, r, M, T/M),
               payoff = std::max(K - S0,0.);

        f << S0 << ' ' << VEuropeanPut << ' ' << VAmericanPut << ' ' << payoff << '\n';
    }
    f.close();
}

// enable doxygen processing for this header:
/** @file */