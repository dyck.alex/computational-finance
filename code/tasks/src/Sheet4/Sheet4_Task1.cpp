/*!
 *  \date      30. June 2017 .
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */

#include "../tasks.h"
#include <iostream>
#include <cmath>
#include <vector>


/**
 * Binomial Method for a European Call option
 * @param V Value function
 * @param S Process
 * @param S0 Initial price
 * @param K Strike price
 * @param sigma variance
 * @param r risk factor
 * @param M level
 * @param delta_t timestep
 * @return A fair price for an European Call option
 */
double BinomialMethod_EuropeanCall(std::vector<std::vector<double> > &V, std::vector<std::vector<double> > &S,
								   double S0, double K, double sigma, double r, double M, double delta_t) {
	double beta = (std::exp((-r)*delta_t) + std::exp((r+sigma*sigma)*delta_t))*0.5;
	double u = beta + std::sqrt(beta*beta-1);
	double d = beta - std::sqrt(beta*beta-1);
	double p = (std::exp(r*delta_t)-d) / (u-d);


	S[0][0] = S0;
	for (int i = 1; i < M+1; i++) {
        for (int j = 0; j <= i; j++) {
            S[j][i] = S[0][0] * pow(u, j) * pow(d, i - j);
        }
    }

	for (int j = 0; j < M+1; j++) {
		V[j][M] = std::max( S[j][M]-K, 0.);
	}
	
	for (int i = M-1; i >= 0; i--)
		for (int j = 0; j <= i; j++) {
			V[j][i] = std::exp((-r)*delta_t)* (p*V[j+1][i+1]+ (1-p)*V[j][i+1]);
		}
	return V[0][0];
}

/**
* Main function
*/
void sheet4_task1() {
    std::vector<std::vector<double> > V,S;

    // setting parameters
    double S0 = 10, K = 10, sigma = 0.2, T= 2, r = 0.02;

    double M[8] = {1,2,4,8,16,32,64,128};
    for (int i = 0; i < 8; i++) {
        V.resize(M[i]+1);
        for (auto it = V.begin(); it != V.end(); it++)
            it->resize(M[i]+1);
        S.resize(M[i]+1);
        for (auto it = S.begin(); it != S.end(); it++)
            it->resize(M[i]+1);
        double VCall = BinomialMethod_EuropeanCall(V, S, S0, K, sigma, r, M[i], T/M[i]);
        if (M[i] == 32) {
            std::ofstream f;
            f.open("s4t1.dat");
            for (auto it = S.begin(); it != S.end(); it++) {
                for (auto it2 = it->begin(); it2!= it->end(); it2++)
                    f << *it2 << ' ';
                f << '\n';
            }
            f.close();
        }
        std::cout << "The price of a European call option at M = " << M[i] << ": " << VCall << std::endl;
    }
}


// enable doxygen processing for this header:
/** @file */