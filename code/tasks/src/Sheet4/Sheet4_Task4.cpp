/*!
 *  \date      1. July 2017
 *  \author    Lisa Beer
 *  \author    Ho-Yin Leung
 *  \author    Alex Dyck
 *
 *
 */


#include "../tasks.h"
#include <iostream>
#include <cmath>
#include <numeric>
#include <vector>
#include <fstream>

/**
 * Binomial Method for a American call option
 * @param V Value function
 * @param S Process
 * @param S0 Initial price
 * @param K Strike price
 * @param sigma variance
 * @param r risk factor
 * @param M level
 * @param delta_t timestep
 * @return A fair price for an American put option
 */
double BinomialMethod_AmericanCall(std::vector<std::vector<double> > &V, std::vector<std::vector<double> > &S,
                                  double S0, double K, double sigma, double r, double M, double delta_t) {
    double beta = (std::exp((-r)*delta_t) + std::exp((r+sigma*sigma)*delta_t))/2.;
    double u = beta + std::sqrt(beta*beta-1);
    double d = beta - std::sqrt(beta*beta-1);
    double p = (std::exp(r*delta_t)-d) / (u-d);


    S[0][0] = S0;
    for (int i = 1; i < M+1; i++) {
        for (int j = 0; j < i+1; j++) {
            S[j][i] = S[0][0] * pow(u, j) * pow(d, i - j);
        }
    }

    for (int j = 0; j < M+1; j++) {
        V[j][M] = std::max( S[j][M] -K, 0.);
    }

    for (int i = M-1; i >= 0; i--)
        for (int j = 0; j < i+1; j++) {
            V[j][i] = std::max(std::max(S[j][i] - K,0.), std::exp((-r)*delta_t)* (p*V[j+1][i+1]+ (1-p)*V[j][i+1]));
        }

    return V[0][0];
}

/**
 * recursive function for computation of estimation in the American put option of the
 * Broadie Glasserman Algorithm
 * @param ran random number generator
 * @param S0 Starting price
 * @param dt time step width
 * @param Ws previuous value of Wiener process
 * @param j current recursion level
 * @param K Strike price
 * @param sigma variance
 * @param r risk factor
 * @param bias_high high biased estimator for V
 * @param bias_low low biased estimator for V
 * @param B Number of Branches
 * @param M Number of time steps
 */
void compute_estimate_value_Put(gsl_rng * ran, double S0, double dt , double Ws,  size_t j , double K, double sigma, double r,
                                 double & bias_high, double & bias_low, size_t B , size_t M ) {
    std::vector<double> b_low(B) , b_high(B), S;
    S.reserve(B);

    double ir = std::exp(-r*dt);

    for (size_t k = 0; k < B; k++) {
        double rnd = NormalInverseCDF(randomNumber01GSL(ran));
        double Wt = Ws + std::sqrt(dt) * rnd;
        S.push_back(S0 * std::exp((r - pow(sigma,2.)*0.5)*dt
                                  + sigma*Wt
        ));
        if (j+1 == M) {
            b_high[k] = std::max( K-S[k]  , 0.);
            b_low[k] = std::max( K-S[k] , 0.);
        }
        else compute_estimate_value_Put(ran, S[k], dt, Wt, j+1, K, sigma, r, b_high[k], b_low[k], B, M);
    }

    if (M== 4) {
        std::ofstream f;
        f.open("simulation_Put.dat", std::ofstream::app);
        f << j << ' ' ;
        for (size_t k = 0; k < B; k++)
            f << S[k] << ' ';
        f << std::endl;
        f.close();
    }

    bias_high = std::accumulate(b_high.begin(), b_high.end(),0);
    bias_high *= ir/B;
    bias_high = std::max( std::max(K-S0, 0.), bias_high);

    std::vector<double> nu;
    nu.reserve(B);
    for (size_t k = 0; k< B ; k++) {
        double tmp_cond = 0;
        for (size_t l = 0; l < B; l++) {
            if (l != k)
                tmp_cond += b_low[l];
        }
        tmp_cond *= ir/(B-1);
        if (std::max(K-S0, 0.) >= tmp_cond)
            nu.push_back(std::max(K-S0, 0.));
        else {
            nu.push_back(ir * b_low[k]);
        }
        bias_low += nu.back();
    }
    bias_low /= B;
}

/**
 * Broadie Glasserman Algorithm for computation of a fair value for an american put option
 * @param ran random number generator
 * @param S0 starting price
 * @param K strike price
 * @param sigma variance
 * @param r risk factor
 * @param delta_t time step width
 * @param B Number of branches
 * @param M Number of time steps
 * @param N Number of iteration to get an average
 * @return Fair Value to an american put option
 */
double BroadieGlassermanAlgorithm_Put(gsl_rng * ran, double S0, double K, double sigma, double r,
                                      double delta_t, size_t B, size_t M , size_t N) {
    double V = 0;
    size_t j = 0;
    for (size_t i = 0; i < N ; i++) {
        double bias_high =0 , bias_low = 0;
        compute_estimate_value_Put(ran, S0, delta_t, 0, j, K ,sigma, r, bias_high, bias_low, B , M);
        V += (bias_high + bias_low)*0.5;
        std::cout << "\r" << (i+1) /(double)N * 100 << "% completed" << std::endl;
    }
    return V/N;
}

/**
 * recursive function for computation of estimation in the American call option of the
 * Broadie Glasserman Algorithm
 * @param ran random number generator
 * @param S0 Starting price
 * @param dt time step width
 * @param Ws previuous value of Wiener process
 * @param j current recursion level
 * @param K Strike price
 * @param sigma variance
 * @param r risk factor
 * @param bias_high high biased estimator for V
 * @param bias_low low biased estimator for V
 * @param B Number of Branches
 * @param M Number of time steps
 */
void compute_estimate_value_Call(gsl_rng * ran, double S0, double dt , double Ws,  size_t j , double K, double sigma, double r,
                                 double & bias_high, double & bias_low, size_t B , size_t M ) {
    std::vector<double> b_low(B,0) ,b_high(B,0), S;
    S.reserve(B);
    double ir = std::exp(-r*dt);
    for (size_t k = 0; k < B; k++) {
        double rnd = NormalInverseCDF(randomNumber01GSL(ran));
        double Wt = Ws + std::sqrt(dt) * rnd;
        (void) Wt;
        S.push_back(S0 * std::exp((r - pow(sigma,2.)*0.5)*dt
                                  + sigma*std::sqrt(dt) * rnd
        ));
        if (j+1 == M) {
            b_high[k] = std::max( S[k] - K  , 0.);
            b_low[k] = std::max( S[k] - K  , 0.);
        }
        else compute_estimate_value_Call(ran, S[k], dt, Wt, j+1, K, sigma, r, b_high[k], b_low[k], B, M);
    }

    if (M== 4) {
        std::ofstream f;
        f.open("simulation_Call.dat", std::ofstream::app);
        f << j << ' ' ;
        for (size_t k = 0; k < B; k++)
            f << S[k] << ' ';
        f << std::endl;
        f.close();
    }

    bias_high = std::accumulate(b_high.begin(), b_high.end(),0);
    bias_high *= ir/B;
    bias_high = std::max( std::max(S0-K, 0.), bias_high);

    std::vector<double> nu;
    nu.reserve(B);
    for (size_t k = 0; k< B ; k++) {
        double tmp_cond = 0;
        for (size_t l = 0; l < B; l++) {
            if (l != k)
                tmp_cond += b_low[l];
        }
        tmp_cond *= ir/(B-1.);
        if (std::max(S0-K, 0.) >= tmp_cond)
            nu.push_back(std::max(S0-K, 0.));
        else {
            nu.push_back(ir * b_low[k]);
        }
        bias_low += nu.back();
    }
    bias_low /= B;
}


/**
 * Broadie Glasserman Algorithm for computation of a fair value for an american call option
 * @param ran random number generator
 * @param S0 starting price
 * @param K strike price
 * @param sigma variance
 * @param r risk factor
 * @param delta_t time step width
 * @param B Number of branches
 * @param M Number of time steps
 * @param N Number of iteration to get an average
 * @return Fair Value to an american call option
 */
double BroadieGlassermanAlgorithm_Call(gsl_rng * ran, double S0, double K, double sigma, double r,
                                      double delta_t, size_t B, size_t M , size_t N) {
    double V = 0;
    size_t j = 0;
    for (size_t i = 0; i < N ; i++) {
        double bias_high = 0 , bias_low = 0;
        compute_estimate_value_Call(ran, S0, delta_t, 0, j, K ,sigma, r, bias_high, bias_low, B , M);
        V += (bias_high + bias_low)*0.5;
        //std::cout << "\r" << (i+1) /(double)N * 100 << "% completed" << std::endl;
        std::cout << bias_high << ' ' << bias_low << std::endl;
    }
    return V/N;
}

/**
 * Main function
 */
void sheet4_task4() {
    std::vector<std::vector<double> > V,S;
    gsl_rng* ran = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(ran,time(NULL));
    // setting parameters
    double S0 = 10, K = 10, sigma = 0.2, T= 2, r = 0.02;

    std::ofstream f;
    f.open("s4t4.dat");
    size_t M[4] = {2,4,5,6}, N = 100 , B = 5;
    for (int i = 0; i < 4; i++) {
        std::cout << M[i] << std::endl;
        V.resize(M[i]+1);
        for (auto it = V.begin(); it != V.end(); it++)
            it->resize(M[i]+1);
        S.resize(M[i]+1);
        for (auto it = S.begin(); it != S.end(); it++)
            it->resize(M[i]+1);

        double VPutBin = BinomialMethod_AmericanPut(V, S, S0, K, sigma, r, M[i], T/M[i]);
        double VPutBG = BroadieGlassermanAlgorithm_Put(ran, S0 , K ,sigma, r, static_cast<double>(T)/M[i],B,M[i],N);
        double VCallBin = BinomialMethod_AmericanCall(V, S, S0, K, sigma, r, M[i], T/M[i]);
        double VCallBG = BroadieGlassermanAlgorithm_Call(ran, S0 , K ,sigma, r, static_cast<double>(T)/M[i],B,M[i],N);
        f << M[i] << ' ' << VPutBG << ' ' << VPutBin << ' ' << std::abs(VPutBG - VPutBin) <<
          ' ' << VCallBG << ' ' << VCallBin << ' ' << std::abs(VCallBG - VCallBin) << '\n';
    }
    f.close();
    gsl_rng_free(ran);
}


// enable doxygen processing for this header:
/** @file */